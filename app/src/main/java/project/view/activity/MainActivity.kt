package project.view.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Telephony
import android.speech.RecognizerIntent
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener
import com.google.firebase.auth.FirebaseAuth
import ir.vahidgarousi.app.payabankv1.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import project.framework.activity.VAppCompatActivity
import project.framework.core.VBase
import project.framework.database.entity.Transaction
import project.framework.database.videmodel.TransactionViewModel
import project.framework.helper.VRequestHelper
import project.view.`interface`.OnSpeechReceived
import project.view.fragment.UserAccountFragment

class MainActivity : VAppCompatActivity() {
    lateinit var ui: Ui

    inner class Ui {

    }


    interface OnTransactionReceived {
        fun onTransactionReceived(sms: Telephony.Sms)
    }

    private lateinit var transactionViewModel: TransactionViewModel
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeActivity()

        navigation.setOnNavigationItemSelectedListener(mOnNavigationBottomItemSelectListner)
    }

    override fun onStart() {
        super.onStart()
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            //startActivity(getLaunchIntent(this))
        }
        navigation.selectedItemId = R.id.main_menu_transaction_list
    }

    private val mOnNavigationBottomItemSelectListner = OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.main_menu_search -> {
                findNavController(
                    this,
                    R.id.nav_host
                ).navigate(R.id.search_dest)
                txt_toolbar_Title.text = "جستجو"
                return@OnNavigationItemSelectedListener true
            }
            R.id.main_menu_transaction_list -> {
                findNavController(
                    this,
                    R.id.nav_host
                ).navigate(R.id.main_dest)
                txt_toolbar_Title.text = "تراکنش ها"
                return@OnNavigationItemSelectedListener true
            }
            R.id.main_menu_category_list -> {
                findNavController(
                    this,
                    R.id.nav_host
                ).navigate(R.id.category_dest)
                txt_toolbar_Title.text = "دسته بندی"
                return@OnNavigationItemSelectedListener true
            }
            R.id.main_menu_user_account -> {
                findNavController(
                    this,
                    R.id.nav_host
                ).navigate(R.id.user_account_dest)
                txt_toolbar_Title.text = "ناحیه کاربری"
                return@OnNavigationItemSelectedListener true
            }
        }
        return@OnNavigationItemSelectedListener false
    }

    private fun test() {
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)
        transactionViewModel.getAll().observe(this,
            Observer<List<Transaction>> {
                for (transaction in it) {
                    Log.i("LOGGER", "" + transaction.toString())
                }
            })
    }

    private fun initializeActivity() {
        ui = Ui()
        VAppCompatActivity.Founder(this)
            .noActionbar()
            .contentView(R.layout.activity_main)
            .extractUi(ui)
            .build()
        VBase.setCurrentActivity(this)
        VBase.setSupportFragmentManager(supportFragmentManager)
        VRequestHelper.requestPermissions()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            100 -> {
                if (resultCode == Activity.RESULT_OK && null != data) {
                    val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    Log.i("LOGGER", "" + result[0])
                    onSpeechReceived.onSpeechReceived(result[0])
                }
            }
            165 -> {
                //null and exception handling omitted for brevity

                val userChosenUri = data?.data
                val inStream = getDatabasePath("payabank.db").absoluteFile.inputStream()
                val outStream = contentResolver.openOutputStream(userChosenUri)

                inStream.use { input ->
                    outStream.use { output ->
                        input.copyTo(output)
                    }
                }
            }

        }
    }

    companion object {
        private lateinit var onSpeechReceived: OnSpeechReceived
        fun getLaunchIntent(from: Context) = Intent(from, UserAccountFragment::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }

        fun setOnSpeechReceived(onSpeechReceived: OnSpeechReceived) {
            this.onSpeechReceived = onSpeechReceived
        }
    }

    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(VBase.getContext(), "جهت خروج از برنامه دوبار دکمه بازگشت را فشار دهید", Toast.LENGTH_LONG)
            .show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

}