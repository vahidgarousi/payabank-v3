package project.view.`interface`

interface OnPriceChanged {
    fun onPriceChange(newPrice: String)
}