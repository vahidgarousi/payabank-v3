package project.view.`interface`

import project.framework.database.entity.Transaction

interface OnTransactionEditClickListener {
    fun onTransactionEditClick(transaction: Transaction)
}