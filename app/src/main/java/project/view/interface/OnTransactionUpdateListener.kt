package project.view.`interface`

import project.framework.database.entity.Transaction

interface OnTransactionUpdateListener {
    fun onTransactionUpdated(transaction: Transaction)
}

