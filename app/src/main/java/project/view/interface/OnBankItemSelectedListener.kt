package project.view.`interface`

import project.framework.database.entity.Bank

interface OnBankItemSelectedListener {
    fun onBankItemSelect(bank: Bank)
}