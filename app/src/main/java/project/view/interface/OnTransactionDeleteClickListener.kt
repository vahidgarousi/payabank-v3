package project.view.`interface`

import project.framework.database.entity.Transaction

interface OnTransactionDeleteClickListener {
    fun onTransactionDeleteClick(transaction: Transaction)
}