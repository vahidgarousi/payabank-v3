package project.view.`interface`

interface OnSpeechReceived {
    fun onSpeechReceived(result: String)
}