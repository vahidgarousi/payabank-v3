package project.view.fragment


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import ir.vahidgarousi.app.payabankv1.R
import kotlinx.android.synthetic.main.fragment_search.*
import project.core.adapter.TransactionListAdapter
import project.framework.core.VBase
import project.framework.database.entity.Transaction
import project.framework.database.videmodel.TransactionViewModel
import project.framework.helper.Common
import project.view.`interface`.OnTransactionEditClickListener

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SearchFragment : Fragment() {

    private lateinit var transactionViewModel: TransactionViewModel
    private lateinit var transactionListAdapter: TransactionListAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
        val transactionEditClickListener = object : OnTransactionEditClickListener {
            override fun onTransactionEditClick(transaction: Transaction) {
                Common.vLog(transaction.toString())
            }
        }
        val onTransactionClickListener = object : OnTransactionEditClickListener {
            override fun onTransactionEditClick(transaction: Transaction) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        }
        transactionListAdapter =
            TransactionListAdapter(
                R.layout.item_transaction_in_category,
                transactionEditClickListener
            )
        edt_searchFragment_textSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(sentence: CharSequence?, start: Int, before: Int, count: Int) {
                transactionViewModel.filterResultByInput(sentence.toString())
                    .observe(VBase.getLifecycleOwner(),
                        Observer {
                            if (it.size == 0) {
                                Toast.makeText(VBase.getContext(), "no data", Toast.LENGTH_LONG).show()
                            }
                            transactionListAdapter.submitList(it)
                        })
            }
        })

        edt_searchFragment_dateSearch.setOnClickListener {

        }
        rcv_searchFragment_result.layoutManager = LinearLayoutManager(VBase.getContext())
        rcv_searchFragment_result.adapter = transactionListAdapter
    }

    private fun initViewModels() {
        transactionViewModel =
            ViewModelProviders.of(VBase.getContext() as FragmentActivity)[TransactionViewModel::class.java]
    }
}
