package project.view.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.vahidgarousi.app.payabankv1.R
import project.core.adapter.TransactionAdapter
import project.framework.core.VBase
import project.framework.database.entity.Bank
import project.framework.database.entity.Transaction
import project.framework.database.videmodel.BankViewModel
import project.framework.database.videmodel.SharedViewModel
import project.framework.database.videmodel.TransactionViewModel
import project.view.`interface`.OnTransactionUpdateListener
import java.util.concurrent.Executor

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment mbank = bankViewModel.getBanyById(transaction.bankId)ust implement the
 * [TransactionFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 *
 */
class TransactionFragment : Fragment() {

    private lateinit var bank: Bank
    private var mList: List<Transaction> = emptyList()
    private lateinit var rcvTransactionFragment: RecyclerView
    private lateinit var transactionViewModel: TransactionViewModel
    private lateinit var bankViewModel: BankViewModel
    private lateinit var sharedViewModel: SharedViewModel

    private lateinit var executor: Executor
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rcvTransactionFragment = view.findViewById(R.id.rcvTransactionFragment)

        // initi ViewModels
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)
        bankViewModel = ViewModelProviders.of(this).get(BankViewModel::class.java)
        val listener = object : OnTransactionUpdateListener {
            override fun onTransactionUpdated(transaction: Transaction) {
                transactionViewModel.update(transaction)
                Toast.makeText(VBase.getContext(), "     تراکنش با موفقیت ویرایش شد", Toast.LENGTH_SHORT)
                    .show()
            }
        }
        val mAdapter = TransactionAdapter(mList, listener)

        //Get data
        transactionViewModel.getAll().observe(this,
            Observer<List<Transaction>> { list ->
                val bankList = arrayListOf<Bank>()
                for (transaction in list) {
                    bank = bankViewModel.getBanyById(transaction.bankId!!)
                    bankList.add(bank)
                }
                mAdapter.setAdapter(list, bankList)
            })
        mAdapter.notifyDataSetChanged()
        rcvTransactionFragment.apply {
            layoutManager = LinearLayoutManager(VBase.getContext())
            adapter = mAdapter
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.i("LOGGER","requestCode is $requestCode and resultCode is $resultCode and data is $data")
    }
}
