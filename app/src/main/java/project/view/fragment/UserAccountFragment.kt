package project.view.fragment


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_user_account.*
import project.framework.core.VBase
import project.framework.helper.Common
import project.framework.widget.Dialog


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
var account: GoogleSignInAccount? = null

/**
 * A simple [Fragment] subclass.
 *
 */
class UserAccountFragment : Fragment() {
    val RC_SIGN_IN: Int = 1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions
    private lateinit var firebaseAuth: FirebaseAuth
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(ir.vahidgarousi.app.payabankv1.R.layout.fragment_user_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rl_userAccount_loginLogout.setOnClickListener {
            Common.vLog("start")
            configureGoogleSignIn()
            checkIsUserLoggedIn()
        }
        rl_userAccount_export.setOnClickListener {
            //somewhere in the code:
            val intent = Intent(Intent.ACTION_CREATE_DOCUMENT)
            intent.type = "*/* // this line is a must when using ACTION_CREATE_DOCUMENT"
            startActivityForResult(intent, 165)
            Dialog.openSavedSuccessfully()
        }
        rl_userAccount_resote.setOnClickListener {

        }
    }

    private fun checkIsUserLoggedIn() {
        val firebasUser = FirebaseAuth.getInstance().currentUser
        val googleUser = GoogleSignIn.getLastSignedInAccount(VBase.getContext())
        firebaseAuth = FirebaseAuth.getInstance()
        if (firebasUser != null || googleUser != null) {
            signOutFromGoogle()
            updateUI(account)
            Toast.makeText(VBase.getContext(), "شما با موفقیت خارج  شدید", Toast.LENGTH_LONG).show()
        } else {
            signInWithGoogle()
        }
    }

    private fun signOutFromGoogle() {
        Common.vLog("user isn't null")
        firebaseAuth.signOut()
        mGoogleSignInClient.signOut()
        account = null
    }

    private fun signInWithGoogle() {
        Common.vLog("user is null")
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
        firebaseAuth = FirebaseAuth.getInstance()
    }

    override fun onStart() {
        super.onStart()
        val account = GoogleSignIn.getLastSignedInAccount(VBase.getContext())
        updateUI(account)
    }

    private fun configureGoogleSignIn() {
        mGoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(ir.vahidgarousi.app.payabankv1.R.string.default_web_client_id))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(VBase.getCurrentActivity(), mGoogleSignInOptions)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                account = task.getResult(ApiException::class.java)!!
                //firebaseAuthWithGoogle(account!!)
                handleSignInResult(task)
            } catch (e: ApiException) {
                Toast.makeText(VBase.getContext(), "Google sign in failed:(", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
            if (it.isSuccessful) {
                Toast.makeText(
                    VBase.getContext(),
                    " شما با موفقیت وارد شدید${account?.email} کاربر گرامی ",
                    Toast.LENGTH_LONG
                ).show()
                updateUI(account)
            } else {
                Toast.makeText(VBase.getContext(), "ورود با موفقیت انجام نشد", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            account = completedTask.getResult(ApiException::class.java)!!
            Toast.makeText(
                VBase.getContext(),
                " شما با موفقیت وارد شدید${account?.email} کاربر گرامی ",
                Toast.LENGTH_LONG
            ).show()
            // Signed in successfully, show authenticated UI.
            updateUI(account)
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Toast.makeText(VBase.getContext(), "ورود با موفقیت انجام نشد", Toast.LENGTH_LONG).show()
            Log.w("LOGGER", "signInResult:failed code=" + e.statusCode)
            updateUI(null)
        }

    }

    private fun updateUI(account: GoogleSignInAccount?) {
        if (account == null) {
            txt_userAccount_signInOut.text = "ورود به حساب کاربری"
            txt_userAccount_email.text = ""
            txt_userAccount_name.text = ""
            img_userAccount_profileImage.visibility = View.GONE
            return
        }
        txt_userAccount_email.text = account.email.toString()
        txt_userAccount_name.text = account.displayName
        img_userAccount_profileImage.visibility = View.VISIBLE
        Picasso.get().load(account.photoUrl).into(img_userAccount_profileImage)
        txt_userAccount_signInOut.text = "خروج از حساب کاربری"
    }

}
