package project.view.fragment


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.HORIZONTAL
import android.widget.LinearLayout.VERTICAL
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.vahidgarousi.app.payabankv1.R
import kotlinx.android.synthetic.main.fragment_category.*
import project.core.adapter.BankListAdapter
import project.core.adapter.TransactionListAdapter
import project.framework.core.VBase
import project.framework.database.entity.Bank
import project.framework.database.entity.Transaction
import project.framework.database.videmodel.BankViewModel
import project.framework.database.videmodel.TransactionViewModel
import project.framework.widget.Dialog
import project.view.`interface`.OnBankItemSelectedListener
import project.view.`interface`.OnTransactionEditClickListener
import project.view.`interface`.OnTransactionUpdateListener
import project.view.enum.TransactionTypes


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class CategoryFragment : Fragment() {

    private lateinit var rcvCategoryFragmentBankList: RecyclerView
    private lateinit var rcvCategoryFragmentTransactionList: RecyclerView
    private lateinit var bankViewModel: BankViewModel
    private lateinit var transactionViewModel: TransactionViewModel
    private lateinit var bankAdapter: BankListAdapter
    private lateinit var transactionListAdapter: TransactionListAdapter
    private lateinit var onBankItemClickListener: OnBankItemSelectedListener
    private var bankId: Int = 1
    private var bankList: List<Bank> = ArrayList()
    private var transactionType = TransactionTypes.DEPOST
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        setupViewModels()
        initializeBankData()
        initializeTransactionData()
        btn_categoryFragment_typeDeposit.setOnClickListener {
            Toast.makeText(VBase.getContext(), "bankd id is : " + bankId, Toast.LENGTH_LONG).show()
            transactionViewModel.getAllTransactionOrderByBankIdAndTypeId(bankId, TransactionTypes.DEPOST)
                .observe(VBase.getLifecycleOwner(),
                    Observer {
                        if (it != null) {
                            updateAdapter(it)
                            btn_categoryFragment_typeDeposit.background =
                                VBase.get().resources.getDrawable(R.drawable.button_primary)
                            btn_categoryFragment_typeRemoval.background =
                                VBase.get().resources.getDrawable(R.drawable.button_secondary)
                            btn_categoryFragment_typeDeposit.setTextColor(VBase.get().resources.getColor(R.color.white))
                            btn_categoryFragment_typeRemoval.setTextColor(VBase.get().resources.getColor(R.color.colorAccent))
                            transactionType = TransactionTypes.DEPOST
                        }
                    })
        }
        btn_categoryFragment_typeRemoval.setOnClickListener {
            transactionViewModel.getAllTransactionOrderByBankIdAndTypeId(bankId, TransactionTypes.REMOVAL)
                .observe(VBase.getLifecycleOwner(),
                    Observer {
                        updateAdapter(it)
                        btn_categoryFragment_typeRemoval.background =
                            VBase.get().resources.getDrawable(R.drawable.button_primary)
                        btn_categoryFragment_typeDeposit.background =
                            VBase.get().resources.getDrawable(R.drawable.button_secondary)
                        btn_categoryFragment_typeDeposit.setTextColor(VBase.get().resources.getColor(R.color.colorAccent))
                        btn_categoryFragment_typeRemoval.setTextColor(VBase.get().resources.getColor(R.color.white))
                        transactionType = TransactionTypes.REMOVAL
                    })
        }

    }

    private fun updateButtonStyles() {

    }

    private fun updateAdapter(it: List<Transaction>?) {
        transactionListAdapter.submitList(it)
    }

    private fun initializeTransactionData() {

        bankViewModel.getAll().observe(VBase.getLifecycleOwner(),
            Observer {
                bankList = it
            })
        val onTransactionEditBtnClickListener = object : OnTransactionEditClickListener {
            override fun onTransactionEditClick(transaction: Transaction) {
                Dialog.openEditTransactionDialog(VBase.getContext(), transaction, bankList,
                    object : OnTransactionUpdateListener {
                        override fun onTransactionUpdated(transaction: Transaction) {
                            transactionViewModel.update(transaction)
                            //Toast.makeText(VBase.getContext(), "با موفقیت ویرایش شد", Toast.LENGTH_LONG).show()
                            transactionListAdapter.notifyDataSetChanged()
                        }
                    }, object : OnBankItemSelectedListener {
                        override fun onBankItemSelect(bank: Bank) {
                            //Toast.makeText(VBase.getContext(), "" + bank.name, Toast.LENGTH_LONG).show()
                            transaction.bankId = bank.id
                        }
                    })
            }
        }
        transactionListAdapter =
            TransactionListAdapter(
                R.layout.item_transaction_in_category,
                onTransactionEditBtnClickListener
            )
        rcvCategoryFragmentTransactionList.adapter = transactionListAdapter
        transactionViewModel.getAll().observe(VBase.getLifecycleOwner(),
            Observer {
                if (it != null) {
                    transactionListAdapter.submitList(it)
                }
            })
    }

    private fun initializeBankData() {
        onBankItemClickListener = object : OnBankItemSelectedListener {
            override fun onBankItemSelect(bank: Bank) {
                with(transactionViewModel) {
                    getAllTransactionOrderByBankIdAndTypeId(
                        bank.id!!,
                        transactionType
                    ).observe(VBase.getLifecycleOwner(),
                        Observer {
                            bankId = bank.id!!
                            if (it.size == 0) {
                                note_data.visibility = View.VISIBLE
                                rv_categoryFragment_transaction.visibility = View.GONE
                                return@Observer
                            }
                            note_data.visibility = View.GONE
                            rv_categoryFragment_transaction.visibility = View.VISIBLE
                            transactionListAdapter.submitList(it)
                        })
                }
            }
        }

        bankAdapter = BankListAdapter(R.layout.item_bank, onBankItemClickListener)
        rcvCategoryFragmentBankList.adapter = bankAdapter
        bankViewModel.getAll().observe(VBase.getLifecycleOwner(),
            Observer {
                bankAdapter.submitList(it)
            })
    }

    private fun setupViewModels() {
        bankViewModel = ViewModelProviders.of(this).get(BankViewModel::class.java)
        transactionViewModel = ViewModelProviders.of(this).get(TransactionViewModel::class.java)
    }

    @SuppressLint("WrongConstant")
    private fun setupViews() {
        rcvCategoryFragmentBankList = view!!.findViewById(R.id.rv_categoryFragment_bank)
        rcvCategoryFragmentTransactionList = view!!.findViewById(R.id.rv_categoryFragment_transaction)
        rcvCategoryFragmentBankList.layoutManager = LinearLayoutManager(VBase.getContext(), HORIZONTAL, true)
        rv_categoryFragment_transaction.layoutManager = LinearLayoutManager(VBase.getContext(), VERTICAL, false)
    }


}
