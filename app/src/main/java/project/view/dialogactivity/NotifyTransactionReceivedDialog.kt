package project.view.dialogactivity

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.squareup.picasso.Picasso
import ir.vahidgarousi.app.payabankv1.R
import kotlinx.android.synthetic.main.dialog_transaction_reicived.*
import project.Const
import project.Const.Companion.REQ_CODE_SPEECH_INPUT
import project.framework.activity.VAppCompatActivity.Founder
import project.framework.core.VBase
import project.framework.database.entity.Bank
import project.framework.database.entity.Transaction
import project.framework.database.videmodel.BankViewModel
import project.framework.database.videmodel.TransactionViewModel
import project.framework.helper.Common
import project.framework.widget.Dialog
import project.view.`interface`.OnBankItemSelectedListener
import project.view.`interface`.OnSpeechReceived
import project.view.activity.MainActivity

class NotifyTransactionReceivedDialog : AppCompatActivity() {

    private lateinit var ui: Ui

    inner class Ui {

    }

    private lateinit var bankViewModel: BankViewModel
    private fun promptSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "fa_IR")
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, VBase.getContext().getString(R.string.speach_prompt))

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                VBase.getContext(),
                VBase.getContext().getString(R.string.speech_not_supported),
                Toast.LENGTH_SHORT
            ).show();
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ui = Ui()
        Founder(this)
            .noActionbar()
            .noTitleBar()
            .contentView(R.layout.dialog_transaction_reicived)
            .extractUi(ui)
            .build();
        setFinishOnTouchOutside(true)
        val transaction = intent.getSerializableExtra(Const.TRANSACTION_RECEIVED_TRANSACTION) as Transaction
        val bank = intent.getSerializableExtra(Const.TRANSACTION_RECEIVED_BANK) as Bank
        setupViews(transaction, bank)
        bankViewModel = ViewModelProviders.of(VBase.getContext() as FragmentActivity)[BankViewModel::class.java]
        lnBankItemIsSelected.setOnClickListener {
            var mBankList = ArrayList<Bank>()
            bankViewModel.getAll().observe(VBase.getLifecycleOwner(),
                Observer {
                    mBankList = it as ArrayList<Bank>
                })
            Dialog.openChooseBankDialog(
                VBase.getContext(),
                mBankList,
                object : OnBankItemSelectedListener {
                    override fun onBankItemSelect(bank: Bank) {
                        txtTransactionBankSelect.text = bank.name
                        txtTransactionPriceSelect.text = Common.str2ThusandSeprator(transaction.amount)
                        edtTransactionDescription.setText(transaction.description)
                        Picasso.get().load(Uri.parse(bank.imageUrl)).into(img_dialog_bank_logo)
                    }
                })
        }

        MainActivity.setOnSpeechReceived(object : OnSpeechReceived {
            override fun onSpeechReceived(result: String) {
                val value = txtTransactionPriceSelect.text.toString() + " " + result
                txtTransactionPriceSelect.setText(value)
            }
        })

        img_transactionDialog_voice.setOnClickListener {
            promptSpeechInput()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupViews(transaction: Transaction, bank: Bank) {
        updateTransactionDependendOnType(transaction, lnTransactionDeposit, lnTransactionRemoval)
        lnBankItemIsSelected.visibility = View.VISIBLE
        txtBankItemIsNotSelected.visibility = View.GONE
        try {
            Picasso.get().load(Uri.parse(bank.imageUrl)).into(img_dialog_bank_logo)
            txtTransactionBankSelect.text = bank.name
        } catch (e: Exception) {
            Log.e("VERROR", "" + e.message)
        }



        txtTransactionEditBtn.setOnClickListener {
            try {
                val transActionViewModel =
                    ViewModelProviders.of(VBase.getContext() as FragmentActivity)[TransactionViewModel::class.java]
                transaction.description = edtTransactionDescription.text.toString()
                transActionViewModel.insert(transaction)
                recreate()
            } catch (e: Exception) {
                Log.e("LOGGER", e.message.toString())
            }
            Toast.makeText(VBase.getContext(), "تراکنش با موفقیت ثبت شد", Toast.LENGTH_SHORT).show()
            finish()
        }
        txtTransactionTitle.text = "تراکنش جدید"
        edtTransactionDescription.setText(transaction.description)
        txtTransactionDismissBtn.setOnClickListener {
            finish()
        }
        if (transaction.bankId != null) {
            lnBankItemIsSelected.visibility = View.VISIBLE
            txtBankItemIsNotSelected.visibility = View.GONE
        } else {
            /// No Bank
            lnBankItemIsSelected.visibility = View.GONE
            txtBankItemIsNotSelected.visibility = View.VISIBLE
            txtBankItemIsNotSelected.text = VBase.get().resources.getString(R.string.choose_bank_title)
        }
        updateTransactionDependendOnType(transaction, lnTransactionDeposit, lnTransactionRemoval)
        txtTransactionPriceSelect.text = Common.str2ThusandSeprator(transaction.amount) + " تومان"

        transaction.run {
            lnTransactionDeposit.setOnClickListener {
                this.type = "DEPOSIT"
                updateTransactionDependendOnType(transaction, lnTransactionDeposit, lnTransactionRemoval)
            }
            lnTransactionRemoval.setOnClickListener {
                this.type = "REMOVAL"
                updateTransactionDependendOnType(transaction, lnTransactionDeposit, lnTransactionRemoval)
            }
        }

    }

    private fun updateTransactionDependendOnType(
        transaction: Transaction,
        lnDeposit: LinearLayout,
        lnRemoval: LinearLayout
    ) {
        val isTransactionTypeDeposit: Boolean
        if (transaction.type.equals("DEPOSIT")) {
            Log.i("LOGGER", "true")
            isTransactionTypeDeposit = true
        } else {
            Log.i("LOGGER", "false")
            isTransactionTypeDeposit = false
        }
        if (isTransactionTypeDeposit) {
            lnDeposit.setBackgroundColor(VBase.get().resources.getColor(R.color.colorAccent))
            lnRemoval.setBackgroundColor(VBase.get().resources.getColor(R.color.backgroundGray))
        } else {
            lnRemoval.setBackgroundColor(VBase.get().resources.getColor(R.color.colorAccent))
            lnDeposit.setBackgroundColor(VBase.get().resources.getColor(R.color.backgroundGray))
        }
    }
}
