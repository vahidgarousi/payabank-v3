package project.view.dialogactivity

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import butterknife.BindView
import butterknife.BindViews
import butterknife.ButterKnife
import ir.vahidgarousi.app.payabankv1.R
import project.Const.Companion.Price_For_Full_Screen_Dialog
import project.framework.core.VBase
import project.framework.helper.Common.Companion.removeThousandSeprator
import project.framework.helper.Common.Companion.vibrate
import project.view.`interface`.OnPriceChanged


class FullScreenDialog : DialogFragment(), View.OnClickListener {
    var currentValue: String = ""
    var oldValue: String = ""
    lateinit var price: String
    override fun onClick(view: View?) {
        vibrate(60)
        var newValue = ""
        if (view != null) {
            if (view is TextView) {
                newValue = view.text.toString()
                if (txtDialogPrice.text.toString().equals("0")) {
                    if (newValue == "۰") {
                        return
                    }
                }
                if (oldValue.length < 9) {
                    oldValue += newValue
                    updatePriceView(oldValue)
                    return
                } else {
                    txtDialogPrice.startAnimation(AnimationUtils.loadAnimation(VBase.getContext(), R.anim.shake))
                    txt_unit.startAnimation(AnimationUtils.loadAnimation(VBase.getContext(), R.anim.shake))
                    vibrate(100)
                    Toast.makeText(VBase.getContext(), "مقدار نامعتبر", Toast.LENGTH_LONG).show()
                }

            }
        }
    }

    fun updatePriceView(value: String) {
        try {
            if (value.length < 10) {
                Log.i("LOGGER", "value is => $value and lenght is ${value.length}")
                if (value.length > 3) {
                    txtDialogPrice.text = String.format("%,d", value.toInt())
                    return
                }
                txtDialogPrice.text = value
            }
            Log.i("LOGGER", "value is => $value and lenght is ${value.length}")
        } catch (error: NumberFormatException) {
            Log.d("LOGGER", "" + error.message)
        }
    }

    companion object {
        var TAG = "FullScreenDialog"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)

    }

    @BindViews(
        R.id.txt_dialogPrice_0,
        R.id.txt_dialogPrice_1,
        R.id.txt_dialogPrice_2,
        R.id.txt_dialogPrice_3,
        R.id.txt_dialogPrice_4,
        R.id.txt_dialogPrice_5,
        R.id.txt_dialogPrice_6,
        R.id.txt_dialogPrice_7,
        R.id.txt_dialogPrice_8,
        R.id.txt_dialogPrice_9,
        R.id.txt_dialogPrice_000
    )
    lateinit var buttons: MutableList<View>

    @BindView(R.id.txt_priceDialog_price)
    lateinit var txtDialogPrice: TextView


    @BindView(R.id.txt_unit)
    lateinit var txt_unit: TextView

    @BindView(R.id.img_dialogPrice_backspace)
    lateinit var imgDialogBackSpace: ImageView

    @BindView(R.id.btn_priceDialog_accept)
    lateinit var txtDialogAccept: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        oldValue = ""
        val view = inflater.inflate(R.layout.dialog_price_layout_full_screen, container, false)
        ButterKnife.bind(this, view)
        for (button in buttons) {
            button.setOnClickListener(this)
        }
        imgDialogBackSpace.setOnClickListener {
            vibrate(60)
            if (oldValue.length < 2) {
                vibrate(100)
                updatePriceView("0")
                oldValue = ""
                txtDialogPrice.startAnimation(AnimationUtils.loadAnimation(VBase.getContext(), R.anim.shake))
                txt_unit.startAnimation(AnimationUtils.loadAnimation(VBase.getContext(), R.anim.shake))
                return@setOnClickListener
            }
            Log.i("LOGGER", "oldValue is $oldValue and lenght is ${oldValue.length}")
            oldValue = oldValue.substring(0, oldValue.length - 1)
            updatePriceView(oldValue)
        }

        imgDialogBackSpace.setOnLongClickListener {
            oldValue = ""
            updatePriceView("0")
            return@setOnLongClickListener true
        }
        txtDialogAccept.setOnClickListener {
            vibrate(60)
            val bundle = Bundle()
            bundle.putString(Price_For_Full_Screen_Dialog, oldValue)
            arguments = bundle
            onPriceChanged.onPriceChange(txtDialogPrice.text.toString())
            oldValue = ""
            dismiss()
        }
        price = arguments?.getString(Price_For_Full_Screen_Dialog)!!
        removeThousandSeprator(price)
        updatePriceView(price)
        oldValue = price
        Toast.makeText(VBase.getContext(), "price is $price", Toast.LENGTH_LONG).show()
        return view
    }


    override fun onStart() {
        super.onStart()

        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
            dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        }
    }

    private lateinit var onPriceChanged: OnPriceChanged

    fun onPriceChange(onPriceChanged: OnPriceChanged) {
        this.onPriceChanged = onPriceChanged
    }
}
