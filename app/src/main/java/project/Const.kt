package project

class Const {
    companion object {
        const val TRANSACTION_RECEIVED_TRANSACTION = "TRANSACTION_RECEIVED_TRANSACTION"
        const val TRANSACTION_RECEIVED_BANK = "TRANSACTION_RECEIVED_BANK"
        const val Price_For_Full_Screen_Dialog = "priceForFullScreenDialog"
        const val RC_SIGN_IN = 115
        const val REQ_CODE_SPEECH_INPUT = 100;
    }
}