package project.core.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ir.vahidgarousi.app.payabankv1.R
import project.framework.core.VBase
import project.framework.database.entity.Bank
import project.view.`interface`.OnBankItemSelectedListener


class BankListAdapter(
    @LayoutRes var layoutItemRes: Int,
    private var onBankItemClickListener: OnBankItemSelectedListener
) : ListAdapter<Bank, BankListAdapter.BankHolder>(DIFF_CALLBACK) {
    private var selectedItem = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutItemRes, parent, false)
        return BankHolder(view)
    }

    override fun onBindViewHolder(holder: BankHolder, position: Int) {
        holder.root.setOnClickListener {
            selectedItem = position
            onBankItemClickListener.onBankItemSelect(getItem(position))
            notifyDataSetChanged()
        }
        if (selectedItem == position) {
            holder.bindParams(getItem(position), true)
        } else {
            holder.bindParams(getItem(position), false)
        }

    }

    class BankHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindParams(bank: Bank, isFirstItem: Boolean) {
            txtBankName.text = bank.name
            Picasso.get().load(Uri.parse(bank.imageUrl)).into(imgBankLogo)
            if (isFirstItem) {
                txtBankName.setTextColor(VBase.get().resources.getColor(R.color.textColor))
                root.setBackgroundColor(VBase.get().resources.getColor(R.color.background))
                return
            }
            root.setBackgroundColor(VBase.get().resources.getColor(R.color.backgroundGray))
            txtBankName.setTextColor(VBase.get().resources.getColor(R.color.textColorSecondary))
        }

        var root = itemView
        var txtBankName = itemView.findViewById<TextView>(R.id.txt_itemBank_name)
        var imgBankLogo = itemView.findViewById<ImageView>(R.id.img_itemBank_logo)

    }


    companion object {
        var DIFF_CALLBACK: DiffUtil.ItemCallback<Bank> = object : DiffUtil.ItemCallback<Bank>() {
            override fun areItemsTheSame(oldItem: Bank, newItem: Bank): Boolean {
                return oldItem.id === newItem.id
            }

            override fun areContentsTheSame(oldItem: Bank, newItem: Bank): Boolean {
                return oldItem.equals(newItem)
            }
        }
    }
}