package project.core.adapter

import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ir.vahidgarousi.app.payabankv1.R
import project.core.adapter.BankAdapter.BankViewHolder
import project.framework.database.entity.Bank

class BankAdapter(
    private var bankList: List<Bank>,
    private var onBankItemClickListener: OnBankItemClickListener,
    @LayoutRes var layoutItemRes: Int
) : RecyclerView.Adapter<BankViewHolder>() {
    interface OnBankItemClickListener {
        fun onItemClick(bank: Bank)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(layoutItemRes, parent, false)
        return BankViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bankList.size
    }


    override fun onBindViewHolder(holder: BankViewHolder, position: Int) {
        Log.e("LOGGER", "onBindViewHolder" + holder.txtBankName)
        holder.bindParams(bankList[position])
        holder.root.setOnClickListener {
            onBankItemClickListener.onItemClick(bankList[position])
        }
    }

    class BankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindParams(bank: Bank) {
            txtBankName.text = bank.name
            Picasso.get().load(Uri.parse(bank.imageUrl)).into(imgBankLogo)
        }

        var root = itemView
        var txtBankName = itemView.findViewById<TextView>(R.id.txt_itemBank_name)
        var imgBankLogo = itemView.findViewById<ImageView>(R.id.img_itemBank_logo)
    }


}