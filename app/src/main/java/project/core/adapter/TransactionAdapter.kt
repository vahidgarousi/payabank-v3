package project.core.adapter

import android.annotation.SuppressLint
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import ir.vahidgarousi.app.payabankv1.R
import project.core.adapter.TransactionAdapter.TransactionViewHolder
import project.framework.core.VBase
import project.framework.database.entity.Bank
import project.framework.database.entity.Transaction
import project.framework.helper.VCalendarHelper
import project.framework.widget.Dialog
import project.view.`interface`.OnBankItemSelectedListener
import project.view.`interface`.OnTransactionUpdateListener
import java.util.*

class TransactionAdapter(
    private var transactionList: List<Transaction>,
    private var onTransactionUpdateListener: OnTransactionUpdateListener
) :
    RecyclerView.Adapter<TransactionViewHolder>() {


    private lateinit var bankList: ArrayList<Bank>

    override fun getItemCount(): Int {
        return transactionList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view = VBase.layoutInflate(R.layout.item_transaction, parent, false)
        return TransactionViewHolder(view)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bindTransaction(transactionList[position], bankList[position], bankList, onTransactionUpdateListener)
    }

    fun setAdapter(list: List<Transaction>, bankList: ArrayList<Bank>) {
        this.transactionList = list
        this.bankList = bankList
        notifyDataSetChanged()
    }

    class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SimpleDateFormat", "SetTextI18n")
        fun bindTransaction(
            transaction: Transaction,
            bank: Bank,
            bankList: ArrayList<Bank>,
            onTransactionUpdateListener: OnTransactionUpdateListener
        ) {
            //val date = VCalendarHelper.getYearMonthDayFromDate(transaction.date)
            txtItemTransactionDate.text = VCalendarHelper.getYearMonthDayFromDate(transaction.date)
            txtItemTransactionTime.text = VCalendarHelper.getTime(transaction.date)
            txtItemTransactionBankName.text = transaction.bankId.toString()
            Picasso
                .get()
                .load(Uri.parse(bank.imageUrl))
                .into(imgItemTransactionBankLogo)
            txtItemTransactionMoreDetail.setOnClickListener {
                Dialog.openReceiveTransactionActivityDialog(null)
            }

            txtItemTransactionDescription.text = transaction.description + " " + transaction.name
            txtItemTransactionBankName.text = bank.name
            imgItemTransactionDelete.setOnClickListener {
                Dialog.openDeleteDialog(transaction)
            }
            imgItemTransactionEdit.setOnClickListener {
                updateViews(transaction, bank)
                if (bankList.size != 0) {
                    val onBankItemSelectedListener = object : OnBankItemSelectedListener {
                        override fun onBankItemSelect(bank: Bank) {
                            updateViews(transaction, bank)
                        }
                    }
                    Dialog.openReceiveTransactionDialog(VBase.getContext(), transaction, bankList, onTransactionUpdateListener,onBankItemSelectedListener)
                }
            }
        }


        fun updateViews(transaction: Transaction, bank: Bank) {
            txtItemTransactionDate.text = VCalendarHelper.getYearMonthDayFromDate(transaction.date)
            txtItemTransactionTime.text = VCalendarHelper.getTime(transaction.date)
            txtItemTransactionBankName.text = transaction.bankId.toString()
            try {
                Picasso
                    .get()
                    .load(Uri.parse(bank.imageUrl))
                    .into(imgItemTransactionBankLogo)

            } catch (e: Exception) {
                e.stackTrace
            }
            txtItemTransactionDescription.text = transaction.description + " " + transaction.name
            txtItemTransactionBankName.text = bank.name
        }

        private val txtItemTransactionTime: TextView =
            itemView.findViewById(R.id.txtItemTransactionTime)
        private val txtItemTransactionMoreDetail: TextView =
            itemView.findViewById(R.id.txtItemTransactionMoreDetail)
        private val txtItemTransactionDate: TextView =
            itemView.findViewById(R.id.txtItemTransactionDate)
        private val imgItemTransactionBankLogo: ImageView =
            itemView.findViewById(R.id.imgItemTransactionBankLogo)
        private val imgItemTransactionEdit: ImageView =
            itemView.findViewById(R.id.imgItemTransactionEdit)
        private val imgItemTransactionDelete: ImageView =
            itemView.findViewById(R.id.imgItemTransactionDelete)
        private val txtItemTransactionBankName: TextView =
            itemView.findViewById(R.id.txtItemTransactionBankName)
        private val txtItemTransactionDescription: TextView =
            itemView.findViewById(R.id.txtItemTransactionDescription)

    }


}