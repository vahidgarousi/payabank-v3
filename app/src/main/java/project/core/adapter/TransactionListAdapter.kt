package project.core.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ir.vahidgarousi.app.payabankv1.R
import project.framework.database.entity.Transaction
import project.framework.helper.Common
import project.framework.helper.VCalendarHelper
import project.framework.widget.Dialog
import project.view.`interface`.OnTransactionEditClickListener


class TransactionListAdapter(
    @LayoutRes var layoutItemRes: Int, var onTransactionUpdateListener: OnTransactionEditClickListener) :
    ListAdapter<Transaction, TransactionListAdapter.TransactionViewHolder>(DIFF_CALLBACK) {
    private var selectedItem = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionListAdapter.TransactionViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutItemRes, parent, false)
        return TransactionListAdapter.TransactionViewHolder(view)
    }

    override fun onBindViewHolder(holder: TransactionListAdapter.TransactionViewHolder, position: Int) {
        holder.imgItemTransactionEdit.setOnClickListener {
            onTransactionUpdateListener.onTransactionEditClick(getItem(position))
        }
        holder.root.setOnClickListener {
            selectedItem = position
            notifyDataSetChanged()
        }
        holder.bindParams(getItem(position))

    }

    class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindParams(transaction: Transaction) {
            //val date = VCalendarHelper.getYearMonthDayFromDate(transaction.date)
            txtItemTransactionDate.text = VCalendarHelper.getYearMonthDayFromDate(transaction.date)
            txtItemTransactionTime.text = VCalendarHelper.getTime(transaction.date)
            txtItemTransactionPrice.text = Common.str2ThusandSeprator(transaction.amount)
            txtItemTransactionDescription.text = transaction.description + " " + transaction.name
            imgItemTransactionDelete.setOnClickListener {
                Dialog.openDeleteDialog(transaction)
            }
        }

        var root = itemView
        private val txtItemTransactionTime: TextView =
            itemView.findViewById(R.id.txtItemTransactionTime)
        private val txtItemTransactionDate: TextView =
            itemView.findViewById(R.id.txtItemTransactionDate)
        val imgItemTransactionEdit: ImageView =
            itemView.findViewById(R.id.img_transactionInCategoryFragment_editTransaction)
        val imgItemTransactionDelete: ImageView =
            itemView.findViewById(R.id.imgItemTransactionDelete)
        private val txtItemTransactionDescription: TextView =
            itemView.findViewById(R.id.txt_itemTransactionForCategory_description)
        private val txtItemTransactionPrice: TextView =
            itemView.findViewById(R.id.txt_itemTransactionForCategory_price)
    }


    companion object {
        var DIFF_CALLBACK: DiffUtil.ItemCallback<Transaction> = object : DiffUtil.ItemCallback<Transaction>() {
            override fun areItemsTheSame(oldItem: Transaction, newItem: Transaction): Boolean {
                return oldItem.id === newItem.id
            }

            override fun areContentsTheSame(oldItem: Transaction, newItem: Transaction): Boolean {
                return oldItem.equals(newItem)
            }
        }
    }
}