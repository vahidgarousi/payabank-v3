package project.core.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsMessage
import android.util.Log
import project.framework.core.VBase
import project.framework.helper.Common
import project.framework.widget.Dialog
import project.framework.widget.Dialog.openEditTransactionDialog

class VSmsReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val pdus = intent.extras.get("pdus") as Array<*>
        val sms = SmsMessage.createFromPdu(pdus[0] as ByteArray)
        Common.vLog(sms.originatingAddress)
        if (VBase.getCurrentActivity() != null) {
            openEditTransactionDialog(VBase.getContext(), sms)
            return
        }
        Dialog.openReceiveTransactionActivityDialog(sms)
    }


    fun decodeSms(intent: Intent) {
        val pdus = intent.extras.get("pdus") as Array<*>
        val sms = SmsMessage.createFromPdu(pdus[0] as ByteArray)
        Log.i("LOGGER", "sender is ${sms.originatingAddress}")
        val senderTel = sms.originatingAddress
        val messageBody = sms.messageBody
        // Toast.makeText(VBase.getContext(), senderTel + " : " + messageBody, Toast.LENGTH_LONG).show();
    }
}