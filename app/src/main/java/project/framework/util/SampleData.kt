package project.framework.util

import project.framework.database.entity.Bank
import project.framework.database.entity.Category
import project.framework.database.entity.Transaction
import project.framework.database.entity.TransactionCategory
import project.framework.helper.VDateHelper

class SampleData {
    companion object {
        fun getTransactionCategoryList(): List<TransactionCategory> {
            return listOf(
                TransactionCategory(1, 1),
                TransactionCategory(1, 1)
            )
        }

        fun getBankList(): List<Bank> {
            return listOf(
                Bank("ملی", "700717|20004000", "https://studionamaa.ir/Vahid/PayaBank/bmi-300-c.png"),
                Bank("مسکن", "300014", "https://studionamaa.ir/Vahid/PayaBank/maskan-300-c.png"),
                Bank("شهر", "256398", "https://studionamaa.ir/Vahid/PayaBank/shahr-300-c.png"),
                Bank("تجارت", "7896523", "https://studionamaa.ir/Vahid/PayaBank/tejarat-300-c.png"),
                Bank("کشاورزی", "1651651", "https://studionamaa.ir/Vahid/PayaBank/bki-300-c.png"),
                Bank("صادرات", "700719|20004008", "https://studionamaa.ir/Vahid/PayaBank/bki-300-c.png"),
                Bank("اقتصاد نوین", "50001080", "https://studionamaa.ir/Vahid/PayaBank/en-300-c.png"),
                Bank("اقتصاد جدید", "54164", "https://studionamaa.ir/Vahid/PayaBank/en-300-c.png")
            )
        }

        fun getCategoryList(): List<Category> {
            return listOf(
                Category("خودرو", "008577"),
                Category("خوراک", "00574B"),
                Category("حمل و نقل", "D81B60"),
                Category("قبض", "FFFFFF"),
                Category("بیمه", "ffecb3"),
                Category("اجاره خانه", "ffe082"),
                Category("خدمات دوره ای", "ffd54f"),
                Category("سایر", "ffca28"),
                Category("سلامت خانواده", "fff8e1")
            )
        }

        fun getTransactionList(): List<Transaction> {
            return listOf(
                Transaction("نام تراکنش", VDateHelper.getDate(0), 1, "واریز", "12500", "توضیاحات"),
                Transaction("نام واریزی", VDateHelper.getDate(0), 2, "برداشت", "15000", "دورهمی"),
                Transaction("نام برداشتی", VDateHelper.getDate(0), 2, "برداشت", "15000", "دورهمی")
            )
        }
    }
}

//
//Bank("ملی", "https://studionamaa.ir/Vahid/PayaBank/bmi-300-c.png"),
//Bank("مسکن", "https://studionamaa.ir/Vahid/PayaBank/maskan-300-c.png"),
//Bank("شهر", "https://studionamaa.ir/Vahid/PayaBank/shahr-300-c.png"),
//Bank("تجارت", "https://studionamaa.ir/Vahid/PayaBank/tejarat-300-c.png"),
//Bank("کشاورزی", "https://studionamaa.ir/Vahid/PayaBank/bki-300-c.png"),
//Bank("اقتصاد نوین", "https://studionamaa.ir/Vahid/PayaBank/en-300-c.png")