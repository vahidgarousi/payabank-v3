package project.framework.widget

import android.annotation.SuppressLint
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import ir.vahidgarousi.app.payabankv1.R
import project.core.adapter.BankAdapter
import project.framework.database.entity.Bank
import project.view.`interface`.OnBankItemSelectedListener


class MyBottomSheetDialog(
    context: Context,
    var bankList: List<Bank>,
    var onBankItemSelect: OnBankItemSelectedListener
) : BottomSheetDialog(context), BankAdapter.OnBankItemClickListener {

    override fun onItemClick(bank: Bank) {
        onBankItemSelect.onBankItemSelect(bank)
        dismiss()
    }

    private var rcvBankList: RecyclerView? = null
    private var txtSearch: TextInputEditText? = null
    private var adapter: BankAdapter? = null

    init {
//        if (list == null) {
//            this@MyBottomSheetDialog.list = SampleData.getBankList()
//        }
        create()
    }

    override fun create() {
        val bottomSheetView = layoutInflater.inflate(R.layout.bottom_sheet_layout, null)
        setContentView(bottomSheetView)
        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView.parent as View)
        val bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                Toast.makeText(context, "onStateChanged", Toast.LENGTH_SHORT).show()
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                Toast.makeText(context, "onSlide", Toast.LENGTH_SHORT).show()
            }
        }

        rcvBankList =
            bottomSheetView.findViewById(R.id.rcv_bottom_sheet_layout_bank_list)
        rcvBankList!!.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        adapter = BankAdapter(bankList, this,R.layout.item_bottom_sheet_dialog_bank_list)
        rcvBankList!!.adapter = adapter
        val dividerItemDecoration = DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL)
        rcvBankList!!.addItemDecoration(dividerItemDecoration)
        adapter!!.notifyDataSetChanged()
        txtSearch = bottomSheetView.findViewById(R.id.txt_bottom_sheet_layout_bank_list_search) as TextInputEditText
        txtSearch!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                return
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })

    }

    companion object {

        @SuppressLint("StaticFieldLeak")
        private val instance: MyBottomSheetDialog? = null

        fun getInstance(
            context: Context,
            bankList: List<Bank>,
            onBankItemSelect: OnBankItemSelectedListener
        ): MyBottomSheetDialog {

            return instance
                ?: MyBottomSheetDialog(context, bankList, onBankItemSelect)
        }
    }
}
