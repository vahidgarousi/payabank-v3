package project.framework.widget

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.speech.RecognizerIntent
import android.telephony.SmsMessage
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.squareup.picasso.Picasso
import ir.vahidgarousi.app.payabankv1.R
import project.Const
import project.Const.Companion.Price_For_Full_Screen_Dialog
import project.Const.Companion.REQ_CODE_SPEECH_INPUT
import project.framework.core.VBase
import project.framework.database.entity.Bank
import project.framework.database.entity.Transaction
import project.framework.database.videmodel.BankViewModel
import project.framework.database.videmodel.TransactionViewModel
import project.framework.helper.Common
import project.framework.helper.VCalendarHelper
import project.view.`interface`.OnBankItemSelectedListener
import project.view.`interface`.OnPriceChanged
import project.view.`interface`.OnSpeechReceived
import project.view.`interface`.OnTransactionUpdateListener
import project.view.activity.MainActivity
import project.view.dialogactivity.FullScreenDialog
import project.view.dialogactivity.NotifyTransactionReceivedDialog

object Dialog {
    class Dialog {

    }


    private fun promptSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "fa_IR")
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, VBase.getContext().getString(R.string.speach_prompt))

        try {
            VBase.getCurrentActivity().startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)

        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                VBase.getContext(),
                VBase.getContext().getString(R.string.speech_not_supported),
                Toast.LENGTH_SHORT
            ).show();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    fun openReceiveTransactionDialog(
        context: Context,
        transaction: Transaction?,
        bankList: List<Bank>,
        onTransactionUpdateListener: OnTransactionUpdateListener?,
        onBankItemSelectedListener: OnBankItemSelectedListener?
    ) {

        val dialog = Dialog(context, R.style.theme_sms_receive_dialog)
        dialog.setContentView(R.layout.dialog_transaction_reicived)
        dialog.setCancelable(false)
        val txtEdit = dialog.findViewById<TextView>(R.id.txtTransactionEditBtn)
        val txtDismiss =
            dialog.findViewById<TextView>(R.id.txtTransactionDismissBtn)
        val bankContainer =
            dialog.findViewById<FrameLayout>(R.id.fl_dialog_transaction_bank_container)
        val lnDeposit =
            dialog.findViewById<LinearLayout>(R.id.lnTransactionDeposit)
        val lnRemoval =
            dialog.findViewById<LinearLayout>(R.id.lnTransactionRemoval)
        val txtDescription =
            dialog.findViewById<EditText>(R.id.edtTransactionDescription)
        val txtPrice = dialog.findViewById<TextView>(R.id.txtTransactionPriceSelect)
        val txtBank = dialog.findViewById<TextView>(R.id.txtTransactionBankSelect)
        val lnBankItemIsSelected: LinearLayout =
            dialog.findViewById(R.id.lnBankItemIsSelected)
        val txtBankItemIsNotSelected: TextView =
            dialog.findViewById(R.id.txtBankItemIsNotSelected)
        val imgDialogBankLogo: AppCompatImageView =
            dialog.findViewById(R.id.img_dialog_bank_logo)
        val imgRecordVoice: ImageButton =
            dialog.findViewById(R.id.img_transactionDialog_voice)
        imgRecordVoice.setOnClickListener {
            promptSpeechInput()
        }

        if (onBankItemSelectedListener == null) {
            bankContainer.visibility = View.GONE
        } else {
            bankContainer.visibility = View.VISIBLE
        }
        MainActivity.setOnSpeechReceived(object : OnSpeechReceived {
            override fun onSpeechReceived(result: String) {
                val value = txtDescription.text.toString() + " " + result
                txtDescription.setText(value)
            }
        })
        transaction?.apply {
            updateViewTransactionView(this)
            updateTransactionDependendOnType(this, lnDeposit, lnRemoval)
            lnDeposit.setOnClickListener {
                this.type = "DEPOSIT"
                updateTransactionDependendOnType(this, lnDeposit, lnRemoval)
            }
            lnRemoval.setOnClickListener {
                this.type = "REMOVAL"

                updateTransactionDependendOnType(this, lnDeposit, lnRemoval)
            }
        }

        var selectedBank = Bank()
        for (bank in bankList) {
            if (transaction?.bankId == bank.id) {
                selectedBank = bank
                lnBankItemIsSelected.visibility = View.VISIBLE
                txtBankItemIsNotSelected.visibility = View.GONE
            } else {
                /// No Bank
                lnBankItemIsSelected.visibility = View.GONE
                txtBankItemIsNotSelected.visibility = View.VISIBLE
                txtBankItemIsNotSelected.text =
                    VBase.get().resources.getString(R.string.choose_bank_title)
            }
        }
        if (transaction != null) {
            transaction.run {
                lnBankItemIsSelected.visibility = View.VISIBLE
                txtBankItemIsNotSelected.visibility = View.GONE
                try {
                    Picasso.get().load(Uri.parse(selectedBank.imageUrl)).into(imgDialogBankLogo)
                } catch (e: Exception) {
                    Log.e("VERROR", "" + e.message)
                }
                lnBankItemIsSelected.setOnClickListener {
                    openChooseBankDialog(context, bankList, object : OnBankItemSelectedListener {
                        override fun onBankItemSelect(bank: Bank) {
                            Toast.makeText(
                                VBase.getContext(),
                                "" + selectedBank.imageUrl + selectedBank.id,
                                Toast.LENGTH_LONG
                            ).show()
                            selectedBank = bank
                            txtBank.text = selectedBank.name
                            txtPrice.text = Common.str2ThusandSeprator(transaction.amount)
                            txtDescription.setText(transaction.description)
                            Picasso.get().load(Uri.parse(selectedBank.imageUrl)).into(imgDialogBankLogo)
                            if (onBankItemSelectedListener != null) {
                                onBankItemSelectedListener.onBankItemSelect(selectedBank)
                            }
                        }
                    })
                }
                txtBank.text = selectedBank.name
                txtDescription.setText(description)
                txtPrice.text = "مبلغ " + Common.str2ThusandSeprator(amount)
            }
        }
        txtBank.setOnClickListener {
            openChooseBankDialog(context, bankList,onBankItemSelectedListener!!)
        }

        txtPrice.setOnClickListener {
            val priceDialog = FullScreenDialog()
            val ft = VBase.getFragmentManager().beginTransaction()
            val bundle = Bundle()
            bundle.putString(Price_For_Full_Screen_Dialog, Common.removeThousandSeprator(txtPrice.text.toString()))
            priceDialog.arguments = bundle
            priceDialog.show(ft, FullScreenDialog.TAG)
            priceDialog.onPriceChange(object : OnPriceChanged {
                override fun onPriceChange(newPrice: String) {
                    txtPrice.text = newPrice
                }
            })
        }


        txtEdit.setOnClickListener {
            // ToDo update transaction
            transaction?.bankId = selectedBank.id
            transaction?.description = txtDescription.text.toString().trim()
            transaction?.amount = txtPrice.text.toString().replace("مبلغ ", "")
            onTransactionUpdateListener?.onTransactionUpdated(transaction!!)
            dialog.dismiss()
        }
        txtDismiss.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    private fun updateTransactionDependendOnType(
        transaction: Transaction,
        lnDeposit: LinearLayout,
        lnRemoval: LinearLayout
    ) {
        val isTransactionTypeDeposit: Boolean
        if (transaction.type.equals("DEPOSIT")) {
            Log.i("LOGGER", "true")
            isTransactionTypeDeposit = true
        } else {
            Log.i("LOGGER", "false")
            isTransactionTypeDeposit = false
        }
        if (isTransactionTypeDeposit) {
            lnDeposit.setBackgroundColor(VBase.get().resources.getColor(R.color.colorAccent))
            lnRemoval.setBackgroundColor(VBase.get().resources.getColor(R.color.backgroundGray))
        } else {
            lnRemoval.setBackgroundColor(VBase.get().resources.getColor(R.color.colorAccent))
            lnDeposit.setBackgroundColor(VBase.get().resources.getColor(R.color.backgroundGray))
        }
    }

    fun updateViewTransactionView(transaction: Transaction) {

    }

    fun openReceiveTransactionActivityDialog(sms: SmsMessage?) {

        if (sms != null) {
            // transaction is old
            val smsSender = sms.originatingAddress
            val bankViewModel =
                ViewModelProviders.of(VBase.getCurrentActivity() as FragmentActivity)[BankViewModel::class.java]
            bankViewModel.getAll().observe(VBase.getLifecycleOwner(), Observer { list ->
                for (bank in list) {
                    val bankNumbers = bank.number!!.splitToSequence("|")
                    bankNumbers.forEach { bNumber ->
                        val bankNumber = "98" + bNumber
//                        Log.i("LOGGER", "" + "$bankNumber | $smsSender")
                        if (bankNumber == smsSender) {
                            Log.i("LOGGER", "" + smsSender)
                            val lines = sms.messageBody.split("\\r?\\n".toRegex())

                            var typeDecoded = ""
                            var amount = ""
                            val date = VCalendarHelper.getDate()
                            lines[1].run {
                                amount = Common.str2Number(this)
                                if (lines[1].contains("-")) {
                                    typeDecoded = "REMOVAL"
                                } else if (lines[1].contains("+")) {
                                    typeDecoded = "DEPOSIT"
                                }

                            }
                            val transaction = Transaction(
                                sms.messageBody,
                                date,
                                bank.id!!,
                                typeDecoded,
                                amount,
                                ""
                            )
                            val detailIntent = Intent(VBase.getContext(), NotifyTransactionReceivedDialog::class.java)
                            detailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            detailIntent.putExtra(Const.TRANSACTION_RECEIVED_TRANSACTION, transaction)
                            detailIntent.putExtra(Const.TRANSACTION_RECEIVED_BANK, bank)
                            VBase.getContext().startActivity(detailIntent)
                        }
                    }
                }
            })
        } else {
            // transaction is new
            Log.i("LOGGER", "transaction is new")
        }
//
    }

    fun openChooseBankDialog(
        context: Context,
        bankList: List<Bank>,
        onBankItemSelectedListener: OnBankItemSelectedListener
    ) {
        var selectedBank = Bank()
        val myBottomSheetDialog =
            MyBottomSheetDialog.getInstance(context, bankList, object : OnBankItemSelectedListener {
                override fun onBankItemSelect(bank: Bank) {
                    selectedBank = bank
                    onBankItemSelectedListener.onBankItemSelect(bank)
                }
            })
        myBottomSheetDialog.show()
    }

    fun openDeleteDialog(transaction: Transaction) {
        val dialog = Dialog(VBase.getContext())
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(R.layout.dialog_delete)
        val txtDialogWarningTitle =
            dialog.findViewById<TextView>(R.id.txtDialogWarningTitle)
        val txtTransactionCanel = dialog.findViewById<TextView>(R.id.txtTransactionCanel)
        val txtTransactionDelete =
            dialog.findViewById<TextView>(R.id.txtTransactionDelete)
        txtDialogWarningTitle.setOnClickListener {
            Toast.makeText(VBase.getContext(), "Clicked!", Toast.LENGTH_SHORT).show()
        }
        txtTransactionCanel.setOnClickListener {
            dialog.dismiss()
        }
        txtTransactionDelete.setOnClickListener {
            val transViewModel =
                ViewModelProviders.of(VBase.getContext() as FragmentActivity).get(TransactionViewModel::class.java)
            transViewModel.delete(transaction)
            dialog.dismiss()
        }
        dialog.show()
    }

    fun texxx() {

//
    }

    fun openEditTransactionDialog(
        context: Context,
        transaction: Transaction,
        bankList: List<Bank>,
        onTransactionUpdateListener: OnTransactionUpdateListener,
        onBankItemSelectedListener: OnBankItemSelectedListener
    ) {
        openReceiveTransactionDialog(
            context,
            transaction,
            bankList,
            onTransactionUpdateListener,
            onBankItemSelectedListener
        )
    }

    fun openEditTransactionDialog(context: Context, sms: SmsMessage) {

        if (sms != null) {
            // transaction is old
            val smsSender = sms.originatingAddress
            val bankViewModel =
                ViewModelProviders.of(VBase.getCurrentActivity() as FragmentActivity)[BankViewModel::class.java]
            bankViewModel.getAll().observe(VBase.getLifecycleOwner(), Observer { bankList ->
                for (bank in bankList) {
                    val bankNumbers = bank.number!!.splitToSequence("|")
                    bankNumbers.forEach { bNumber ->
                        val bankNumber = "98" + bNumber
//                        Log.i("LOGGER", "" + "$bankNumber | $smsSender")
                        if (bankNumber == smsSender) {
                            Log.i("LOGGER", "" + smsSender)
                            val lines = sms.messageBody.split("\\r?\\n".toRegex())

                            var typeDecoded = ""
                            var amount = ""
                            val date = VCalendarHelper.getDate()
                            lines[1].run {
                                amount = Common.str2Number(this)
                                if (lines[1].contains("-")) {
                                    typeDecoded = "REMOVAL"
                                } else if (lines[1].contains("+")) {
                                    typeDecoded = "DEPOSIT"
                                }

                            }
                            val transaction = Transaction(
                                sms.messageBody,
                                date,
                                bank.id!!,
                                typeDecoded,
                                amount,
                                ""
                            )
                            //saveTransaction()\
                            val transactionOnUpdateListener = object : OnTransactionUpdateListener {
                                override fun onTransactionUpdated(transaction: Transaction) {
                                    val transactionViewModel =
                                        ViewModelProviders.of(VBase.getContext() as FragmentActivity)[TransactionViewModel::class.java]
                                    transactionViewModel.insert(transaction)
                                }
                            }
                            val onBankItemSelectedListener = object : OnBankItemSelectedListener {
                                override fun onBankItemSelect(bank: Bank) {
                                    transaction.bankId = bank.id
                                }
                            }
                            openReceiveTransactionDialog(
                                context,
                                transaction,
                                bankList,
                                transactionOnUpdateListener,
                                onBankItemSelectedListener
                            )
                        }
                    }
                }
            })
        } else {
            // transaction is new
            Log.i("LOGGER", "transaction is new")
        }
        Toast.makeText(context, sms.messageBody, Toast.LENGTH_LONG).show()
    }

    fun openSavedSuccessfully() {
        val dialog = Dialog(VBase.getContext())
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(R.layout.dailog_save)
        dialog.show()
    }
}