package project.framework.widget;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;

public class VSelect {
  private Activity activity;
  private CharSequence title;
  private CharSequence[] options;
  private CharSequence defaultValue;
  private boolean isCancelable = true;
  private boolean isImmediate = true;
  private int defaultIndex = -1;
  private int selectedIndex = -1;
  private CharSequence okLabel = "Ok";
  private CharSequence cancelLabel = "Cancel";

  public interface OnItemSelectedListener {
    void onItemSelected(int selectedIndex, CharSequence selectedValue);
  }

  private OnItemSelectedListener listener;


  public VSelect(Activity activity) {
    this.activity = activity;
  }

  public VSelect setTitle(CharSequence title) {
    this.title = title;
    return this;
  }

  public VSelect setOptions(CharSequence... options) {
    this.options = options;
    return this;
  }

  public VSelect setIsCancelable(boolean isCancelable) {
    this.isCancelable = isCancelable;
    return this;
  }

  public VSelect setIsImmediate(boolean isImmediate) {
    this.isImmediate = isImmediate;
    return this;
  }

  public VSelect setDefaultIndex(int index) {
    this.defaultIndex = index;
    return this;
  }

  public VSelect setDefaultValue(CharSequence value) {
    this.defaultValue = value;
    return this;
  }


  public VSelect setOkLabel(CharSequence okLabel) {
    this.okLabel = okLabel;
    return this;
  }

  public VSelect setCancelLabel(CharSequence label) {
    this.cancelLabel = label;
    return this;
  }

  public VSelect setOkLabel(int okLabel) {
    this.okLabel = activity.getString(okLabel);
    return this;
  }

  public VSelect setCancelLabel(int cancelLabel) {
    this.cancelLabel = activity.getString(cancelLabel);
    return this;
  }

  public VSelect setOnItemSelectedListener(OnItemSelectedListener listener) {
    this.listener = listener;
    return this;
  }

  public void show() {
    AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
    builder.setTitle(title);
    builder.setCancelable(isCancelable);
    if (defaultValue != null) {
      for (int i = 0; i < options.length; i++) {
        if (options[i].equals(defaultValue)) {
          defaultIndex = i;
          break;
        }
      }
    }

    builder.setSingleChoiceItems(options, defaultIndex, new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        selectedIndex = which;
        if (isImmediate) {
          if (listener != null) {
            listener.onItemSelected(selectedIndex, options[selectedIndex]);
          }
          dialog.dismiss();
        }
      }
    });

    if (!isImmediate) {
      builder.setPositiveButton(okLabel, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          if (listener != null) {
            listener.onItemSelected(selectedIndex, options[selectedIndex]);
          }
        }
      });
    }
    if (isCancelable) {
      builder.setNegativeButton(cancelLabel, null);
    }
    AlertDialog dialog = builder.create();
    dialog.show();
  }
}
