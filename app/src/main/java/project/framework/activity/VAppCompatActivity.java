package project.framework.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import project.framework.core.VBase;

import java.lang.reflect.Field;

public class VAppCompatActivity extends AppCompatActivity {

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
    super.onCreate(savedInstanceState, persistentState);
  }

  @Override
  protected void onResume() {
    super.onResume();
    VBase.setCurrentActivity(this);
  }

  public static class Founder {
    private final Activity activity;
    private int[] features;
    private boolean noTitleBar;
    private boolean fullscreen;
    private boolean noActionbar;
    private int layoutId;
    private Object ui;
    private static final String TAG = "Logger";

    public Founder(Activity activity) {
      this.activity = activity;
    }

    public Founder requestFeature(int... fetures) {
      this.features = fetures;
      return this;
    }

    public Founder noTitleBar() {
      this.noTitleBar = true;
      return this;
    }

    public Founder fullscreen() {
      this.fullscreen = true;
      return this;
    }

    public Founder noActionbar() {
      this.noActionbar = true;
      return this;
    }

    public Founder contentView(@LayoutRes int layoutResId) {
      this.layoutId = layoutResId;
      return this;
    }

    public Founder extractUi(Object ui) {
      this.ui = ui;
      return this;
    }

    public Founder build() {
      if (features != null) {
        for (int feature : features) {
          activity.getWindow().requestFeature(feature);
        }

      }
      if (noTitleBar) {
        activity.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
      }
      if (noActionbar) {
        activity.getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        {
          ActionBar actionBar = activity.getActionBar();
          if (actionBar != null) {
            actionBar.hide();
          }
        }


        if (activity instanceof AppCompatActivity) {
          AppCompatActivity castedActivity = (AppCompatActivity) this.activity;
          androidx.appcompat.app.ActionBar actionBar = castedActivity.getSupportActionBar();
          if (actionBar != null) {
            actionBar.hide();
          }
        }
      }

      if (fullscreen) {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
      }

      activity.setContentView(layoutId);
      Log.i(TAG, "build: " + ui.getClass());

      // reflect ui element
      {
        Class clazz = ui.getClass();
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
          String name = field.getName();
          Class type = field.getType();
          if (name.contains("$") || name.equals("serialVersionUID")) {
            continue;
          }
          Log.i(TAG, "build: " + name);
          int id = VBase.get().getResources().getIdentifier(name, "id", VBase.get().getPackageName());
          try {
            field.set(ui, activity.findViewById(id));
          } catch (IllegalAccessException e) {
            e.printStackTrace();
          }
        }
      }

      return this;
    }
  }
}
