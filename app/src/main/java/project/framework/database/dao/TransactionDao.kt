package project.framework.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import project.framework.database.entity.Transaction


@Dao
interface TransactionDao {

    @get:Query("SELECT * from `transaction` ORDER BY `transaction_id` ASC")
    val allTransaction: LiveData<List<Transaction>>

    @Update
    fun update(transaction: Transaction)

    @Insert
    fun insert(transaction: Transaction)

    @Insert
    fun insertAll(transactionList: List<Transaction>)

    @Delete
    fun delete(transaction: Transaction)

    @Query("DELETE FROM `transaction`")
    fun deleteAll()

//    @Query("SELECT * FROM `transaction` LEFT  JOIN `bank` ON `transaction`.transaction_id = `bank`.bank_id")
//    fun getTransactionAndBank(): LiveData<List<TAndB>>

    @Query("SELECT COUNT(*) FROM `transaction` LEFT  JOIN `bank` ON `transaction`.transaction_id = `bank`.bank_id")
    fun getTransactionAndBankCount(): Int

    @Query("SELECT * FROM `transaction` WHERE type = :type")
    fun getAllTransactionOrderByType(type: String): LiveData<List<Transaction>>

    @Query("SELECT * FROM `transaction` WHERE bankId = :bankId")
    fun getAllTransactionOrderByBankId(bankId: Int?): LiveData<List<Transaction>>

    @Query("SELECT * FROM `transaction` WHERE name Or description LIKE '%' || :query || '%'")
    fun filterResultByInput(query: String): LiveData<List<Transaction>>

    @Query("SELECT * FROM `transaction` WHERE bankId =:bankId AND type = :type")
    fun getAllTransactionOrderByBankIdAndTypeId(bankId: Int, type: String): LiveData<List<Transaction>>
}