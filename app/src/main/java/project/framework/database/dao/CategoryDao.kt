package project.framework.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import project.framework.database.entity.Category


@Dao
interface CategoryDao {

    @get:Query("SELECT * from `category` ORDER BY category_id ASC")
    val allCategory: LiveData<List<Category>>

    @Update
    fun update(category: Category)

    @Insert
    fun insert(category: Category)

    @Insert
    fun insertAll(categoryList: List<Category>)

    @Delete
    fun delete(category: Category)

    @Query("DELETE FROM `category`")
    fun deleteAll()
}