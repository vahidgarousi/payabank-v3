package project.framework.database.daotransaction_category

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import project.framework.database.entity.TransactionCategory


@Dao
interface TransactionCategoryDao {

    @Insert
    fun insert(transactionCategory: TransactionCategory)

    @Insert
    fun insertAll(transactionCategoryList: List<TransactionCategory>)

    @get:Query("SELECT * from transaction_category")
    val allTransactionWithCategory: LiveData<List<TransactionCategory>>
}