package project.framework.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import project.framework.database.entity.Bank


@Dao
interface BankDao {

    @Query("SELECT * from `bank` ORDER BY bank_id ASC")
    fun allBank(): LiveData<List<Bank>>

    @Update
    fun update(bank: Bank)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(bank: Bank)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(bankList: List<Bank>)

    @Delete
    fun delete(bank: Bank)

    @Query("DELETE FROM `bank`")
    fun deleteAll()



    @Query("SELECT * FROM bank WHERE bank_id = :id")
    fun getBankById(id: Int): Bank


    @Query("SELECT COUNT(*) FROM bank")
    fun getBankCount(): Int
}