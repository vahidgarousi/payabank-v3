package project.framework.database.core

import android.annotation.SuppressLint
import project.framework.database.entity.Bank
import project.framework.database.entity.Category
import project.framework.database.entity.Transaction
import project.framework.database.entity.TransactionCategory
import project.framework.helper.VCalendarHelper

class PopulateDatabase {
    companion object {
        fun populateTransactionCategoryData(): List<TransactionCategory> {
            return listOf(
                TransactionCategory(1, 1),
                TransactionCategory(2, 2)
            )
        }

        fun populateBankData(): List<Bank> {
            return listOf(
                Bank("ملی", "700717|20004000", "https://studionamaa.ir/Vahid/PayaBank/bmi-300-c.png"),
                Bank("مسکن", "300014", "https://studionamaa.ir/Vahid/PayaBank/maskan-300-c.png"),
                Bank("شهر", "256398", "https://studionamaa.ir/Vahid/PayaBank/shahr-300-c.png"),
                Bank("تجارت", "7896523", "https://studionamaa.ir/Vahid/PayaBank/tejarat-300-c.png"),
                Bank("کشاورزی", "1651651", "https://studionamaa.ir/Vahid/PayaBank/bki-300-c.png"),
                Bank("صادرات", "700719|20004008", "https://studionamaa.ir/Vahid/PayaBank/bki-300-c.png"),
                Bank("اقتصاد نوین", "50001080", "https://studionamaa.ir/Vahid/PayaBank/en-300-c.png"),
                Bank("اقتصاد جدید", "54164", "https://studionamaa.ir/Vahid/PayaBank/en-300-c.png")
            )
        }

        fun populateCategoryData(): List<Category> {
            return listOf(
                Category("خودرو", "008577"),
                Category("خوراک", "00574B"),
                Category("حمل و نقل", "D81B60"),
                Category("قبض", "FFFFFF"),
                Category("بیمه", "ffecb3"),
                Category("اجاره خانه", "ffe082"),
                Category("خدمات دوره ای", "ffd54f"),
                Category("سایر", "ffca28"),
                Category("سلامت خانواده", "fff8e1")
            )
        }

        @SuppressLint("SimpleDateFormat")
        fun populateTransactionData(): List<Transaction> {
            val date = VCalendarHelper.getDate()
            return listOf(
                Transaction("1تراکنش", date, 1, "DEPOSIT", "12500", "توضیاحات"),
                Transaction("وا2ریزی", date, 2, "REMOVAL", "15000", "دورهمی"),
                Transaction("نام 3واریزی", date, 1, "REMOVAL", "15000", "دورهمی"),
                Transaction("نام وا4ریزی", date, 2, "REMOVAL", "15000", "دورهمی"),
                Transaction("نام 5واریزی", date, 1, "REMOVAL", "15000", "دورهمی"),
                Transaction("نام و6اریزی", date, 2, "REMOVAL", "15000", "دورهمی"),
                Transaction("نام بر8داشتی", date, 3, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام بر8داشتی", date, 4, "REMOVAL", "15000", "دورهمی"),
                Transaction("نام بر9داشتی", date, 1, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام ب10رداشتی", date, 1, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام بر11داشتی", date, 2, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام ب12رداشتی", date, 5, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام 13برداشتی", date, 3, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام 14برداشتی", date, 4, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام ب15رداشتی", date, 5, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام بر16داشتی", date, 1, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام بر17داشتی", date, 6, "REMOVAL", "15000", "دورهمی"),
                Transaction("نام بر18داشتی", date, 1, "DEPOSIT", "15000", "دورهمی"),
                Transaction("نام بر19داشتی", date, 4, "REMOVAL", "15000", "دورهمی")
            )
        }
    }
}