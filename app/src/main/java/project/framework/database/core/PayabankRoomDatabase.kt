package project.framework.database.core

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import project.framework.database.dao.BankDao
import project.framework.database.dao.CategoryDao
import project.framework.database.dao.TransactionDao
import project.framework.database.daotransaction_category.TransactionCategoryDao
import project.framework.database.entity.Bank
import project.framework.database.entity.Category
import project.framework.database.entity.Transaction
import project.framework.database.entity.TransactionCategory
import project.framework.database.typeconveerter.DataConverter
import java.util.concurrent.Executors


@Database(
    entities = [Transaction::class, Category::class, Bank::class, TransactionCategory::class],
    version = 1
)
@TypeConverters(DataConverter::class)
abstract class PayabankRoomDatabase : RoomDatabase() {

    abstract fun transactionDao(): TransactionDao

    abstract fun categoryDao(): CategoryDao

    abstract fun bankDao(): BankDao

    abstract fun transactionCategoryDao(): TransactionCategoryDao

    companion object {

        @Volatile
        private var instance: PayabankRoomDatabase? = null
        private const val DATABASE_NAME = "payabank.db"

        private val LOCK = Object()


        fun getInstance(context: Context): PayabankRoomDatabase {
            if (instance == null) {
                synchronized(LOCK) {
                    if (instance == null) {
                        instance = Room.databaseBuilder(
                            context.applicationContext,
                            PayabankRoomDatabase::class.java, DATABASE_NAME
                        )
                            .fallbackToDestructiveMigration()
                            .allowMainThreadQueries()
                            .addCallback(object : RoomDatabase.Callback() {
                                override fun onCreate(db: SupportSQLiteDatabase) {
                                    super.onCreate(db)
                                    Executors.newSingleThreadExecutor().execute {
                                        getInstance(context).categoryDao()
                                            .insertAll(PopulateDatabase.populateCategoryData())
                                        getInstance(context).bankDao().insertAll(PopulateDatabase.populateBankData())
                                        getInstance(context).transactionDao()
                                            .insertAll(PopulateDatabase.populateTransactionData())
                                        getInstance(context).transactionCategoryDao()
                                            .insertAll(PopulateDatabase.populateTransactionCategoryData())
                                    }
                                }
                            })
                            .setJournalMode(JournalMode.TRUNCATE)
                            .build()

                    }
                }
            }

            return instance as PayabankRoomDatabase
        }
    }
}