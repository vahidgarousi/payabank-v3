package project.framework.database.repositoryLiveData

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import project.framework.database.core.PayabankRoomDatabase
import project.framework.database.dao.TransactionDao
import project.framework.database.entity.Transaction
import project.framework.util.SampleData
import java.util.concurrent.Executors

class TransactionRepository(application: Application) {

    val transactionJoinBankCount: Int
    private val mTransactionDao: TransactionDao
    val allTransaction: LiveData<List<Transaction>>
    //  val transactionJoinBank: LiveData<List<TAndB>>
    private var mDb: PayabankRoomDatabase
    private val executor = Executors.newSingleThreadExecutor()

    val transactionCount: Int?
        get() = null

    init {
        mDb = PayabankRoomDatabase.getInstance(application)
        mTransactionDao = mDb.transactionDao()
        allTransaction = mTransactionDao.allTransaction
        //  transactionJoinBank = mTransactionDao.getTransactionAndBank()
        transactionJoinBankCount = mTransactionDao.getTransactionAndBankCount()
    }


    fun insert(transaction: Transaction) {
        insertAsyncTask(mTransactionDao).execute(transaction)
    }

    fun delete(transaction: Transaction) {
        executor.execute({ mDb.transactionDao().delete(transaction) })
        //Todo When we have two item delete crash with this error :
        /* Error Code : 787 (SQLITE_CONSTRAINT_FOEIGNKEY)
         Caused By : Abort due to constraint violation.
         (FOREIGN KEY constraint failed (code 787))*/
    }

    fun addAllTransaction() {
        executor.execute { mDb.transactionDao().insertAll(SampleData.getTransactionList()) }
    }

    fun update(transaction: Transaction) {
        executor.execute {
            mDb.transactionDao().update(transaction)
        }
    }

    fun getAllTransactionOrderByType(type: String): LiveData<List<Transaction>> {
        return mDb.transactionDao().getAllTransactionOrderByType(type)
    }

    fun getAllTransactionOrderByBankId(bankId: Int?): LiveData<List<Transaction>> {
        return mDb.transactionDao().getAllTransactionOrderByBankId(bankId)
    }

    fun filterResultByInput(query: String): LiveData<List<Transaction>> {
        return mDb.transactionDao().filterResultByInput(query)
    }

    fun getAllTransactionOrderByBankIdAndTypeId(bankId: Int, type: String): LiveData<List<Transaction>> {
        return mDb.transactionDao().getAllTransactionOrderByBankIdAndTypeId(bankId, type)
    }

    private class deleteAsyncTask internal constructor(private val mAsyncTaskDao: TransactionDao) :
        AsyncTask<Transaction, Void, Void>() {

        override fun doInBackground(vararg params: Transaction): Void? {
            mAsyncTaskDao.delete(params[0])
            return null
        }
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: TransactionDao) :
        AsyncTask<Transaction, Void, Void>() {

        override fun doInBackground(vararg params: Transaction): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }

}