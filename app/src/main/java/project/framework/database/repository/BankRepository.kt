package project.framework.database.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import project.framework.database.core.PayabankRoomDatabase
import project.framework.database.dao.BankDao
import project.framework.database.entity.Bank
import project.framework.util.SampleData
import java.util.concurrent.Executors

class BankRepository(application: Application) {

    private val mBankDao: BankDao
    val allBank: LiveData<List<Bank>>
    private val executor = Executors.newSingleThreadExecutor()
    private val mDb: PayabankRoomDatabase

    val bankCount: Int?
        get() = mDb.bankDao().getBankCount()

    init {
        mDb = PayabankRoomDatabase.getInstance(application)
        mBankDao = mDb.bankDao()
        allBank = mBankDao.allBank()
    }


    fun insert(category: Bank) {
        insertAsyncTask(mBankDao).execute(category)
    }

    fun addAll() {
        executor.execute { mDb.bankDao().insertAll(SampleData.getBankList()) }
    }

    fun delete(bank: Bank) {
        executor.execute { mDb.bankDao().delete(bank) }
    }

    fun getBanyById(id: Int): Bank {
        return mDb.bankDao().getBankById(id)
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: BankDao) :
        AsyncTask<Bank, Void, Void>() {

        override fun doInBackground(vararg params: Bank): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }
}