package project.framework.database.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import project.framework.database.core.PayabankRoomDatabase
import project.framework.database.daotransaction_category.TransactionCategoryDao
import project.framework.database.entity.TransactionCategory
import project.framework.util.SampleData
import java.util.concurrent.Executors

class TransactionCategoryRepository(application: Application) {

    private val mTransactionCategoryDao: TransactionCategoryDao
    val allTransactionCategory: LiveData<List<TransactionCategory>>
    private val mDb: PayabankRoomDatabase
    private val executor = Executors.newSingleThreadExecutor()

    val transactionCategoryCount: Int?
        get() = null

    init {
        mDb = PayabankRoomDatabase.getInstance(application)
        mTransactionCategoryDao = mDb.transactionCategoryDao()
        allTransactionCategory = mTransactionCategoryDao.allTransactionWithCategory
    }


    fun insert(transactionCategory: TransactionCategory) {
        insertAsyncTask(mTransactionCategoryDao).execute(transactionCategory)
    }

    fun delete(transactionCategory: TransactionCategory) {

    }

    fun addAll() {
        executor.execute { mDb.transactionCategoryDao().insertAll(SampleData.getTransactionCategoryList()) }
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: TransactionCategoryDao) :
        AsyncTask<TransactionCategory, Void, Void>() {

        override fun doInBackground(vararg params: TransactionCategory): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }
}