package project.framework.database.repository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import project.framework.database.core.PayabankRoomDatabase
import project.framework.database.dao.CategoryDao
import project.framework.database.entity.Category
import project.framework.util.SampleData
import java.util.concurrent.Executors

class CategoryRepository(application: Application) {

    private val mCategoryDao: CategoryDao
    val allCategory: LiveData<List<Category>>
    private val executor = Executors.newSingleThreadExecutor()
    private var mDb: PayabankRoomDatabase

    val categoryCount: Int?
        get() = null

    init {
        mDb = PayabankRoomDatabase.getInstance(application)
        mCategoryDao = mDb.categoryDao()
        allCategory = mCategoryDao.allCategory
    }


    fun insert(category: Category) {
        insertAsyncTask(mCategoryDao).execute(category)
    }

    fun delete(category: Category) {

    }

    fun addAll() {
        executor.execute { mDb.categoryDao().insertAll(SampleData.getCategoryList()) }
    }

    private class insertAsyncTask internal constructor(private val mAsyncTaskDao: CategoryDao) :
        AsyncTask<Category, Void, Void>() {

        override fun doInBackground(vararg params: Category): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }
}