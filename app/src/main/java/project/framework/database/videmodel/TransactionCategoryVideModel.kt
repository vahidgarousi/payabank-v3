package project.framework.database.videmodel

import android.app.Application
import android.content.ClipData
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import project.framework.database.entity.TransactionCategory
import project.framework.database.repository.TransactionCategoryRepository

class TransactionCategoryVideModel(application: Application) : AndroidViewModel(application) {
    private var repository: TransactionCategoryRepository = TransactionCategoryRepository(application)
    private val selected = MutableLiveData<ClipData.Item>()

    fun select(item: ClipData.Item) {
        selected.value = item
    }

    fun insert(transactionCategory: TransactionCategory) {
        repository.insert(transactionCategory)
    }

    fun deleteAll() {
        //repository.delete()
    }
    fun delete(transactionCategory: TransactionCategory) {
        repository.delete(transactionCategory)
    }

    fun getAll(): LiveData<List<TransactionCategory>> {
        return loadAll()
    }

    private fun loadAll(): LiveData<List<TransactionCategory>> {
        return repository.allTransactionCategory
    }

    fun addAllTransaction() {
        repository.addAll()
    }

    fun getTransactionCount(): Int? {
        return repository.transactionCategoryCount
    }


}