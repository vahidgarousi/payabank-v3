package project.framework.database.videmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import project.framework.database.entity.Bank
import project.framework.database.entity.Transaction
import project.framework.database.repository.BankRepository
import project.framework.database.repositoryLiveData.TransactionRepository
import java.util.concurrent.Executors

class SharedViewModel(application: Application) : AndroidViewModel(application) {
    private var mTransactionRepository: TransactionRepository = TransactionRepository(application)
    private var mBankRepository: BankRepository = BankRepository(application)
    private val bank = MutableLiveData<Bank>()
    private val bankList = MutableLiveData<List<Bank>>()
    private var executor = Executors.newSingleThreadExecutor()
    private val transaction = MutableLiveData<Transaction>()
    private val transactionList = MutableLiveData<List<Transaction>>()


    fun setBank(bank: Bank) {
        this.bank.value = bank
    }

    fun getBank(): LiveData<Bank> {
        return this.bank
    }

    fun setBankList(bankList: List<Bank>) {
        this.bankList.value = bankList
    }

    fun getBankList(): LiveData<List<Bank>> {
        executor.execute {
            bankList.postValue(mBankRepository.allBank.value)
        }
        return bankList
    }

    fun setTransaction(transaction: Transaction) {
        this.transaction.value = transaction
    }

    fun getTransaction(): LiveData<Transaction> {
        return transaction
    }

    fun setTransactionList(transactionList: List<Transaction>) {
        this.transactionList.value = transactionList
    }

    fun getTransactionList(): LiveData<List<Transaction>> {
        executor.execute {
            val allTransaction = mTransactionRepository.allTransaction
            transactionList.postValue(allTransaction.value)
        }
        return transactionList
    }
}
