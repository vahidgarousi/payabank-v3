package project.framework.database.videmodel

import android.app.Application
import android.content.ClipData
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import project.framework.database.entity.Category
import project.framework.database.repository.CategoryRepository

class CategoryViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: CategoryRepository = CategoryRepository(application)
    private val selected = MutableLiveData<ClipData.Item>()

    fun select(item: ClipData.Item) {
        selected.value = item
    }

    fun insert(category: Category) {
        repository.insert(category)
    }

    fun deleteAllTransaction() {
        //repository.delete()
    }

    fun delete(category: Category) {
        repository.delete(category)
    }

    fun getAllCategory(): LiveData<List<Category>> {
        return loadCategory()
    }

    private fun loadCategory(): LiveData<List<Category>> {
        return repository.allCategory
    }

    fun addAllTransaction() {
        repository.addAll()
    }

    fun getTransactionCount(): Int? {
        return repository.categoryCount
    }


}