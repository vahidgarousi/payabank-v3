package project.framework.database.videmodel

import android.app.Application
import android.content.ClipData
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import project.framework.database.entity.Bank
import project.framework.database.repository.BankRepository

class BankViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: BankRepository = BankRepository(application)
    private val selected = MutableLiveData<ClipData.Item>()

    fun select(item: ClipData.Item) {
        selected.value = item
    }

    fun insert(bank: Bank) {
        repository.insert(bank)
    }

    fun deleteAll() {
        //repository.delete()
    }

    fun delete(bank: Bank) {
        repository.delete(bank)
    }

    fun getAll(): LiveData<List<Bank>> {
        return loadBank()
    }

    private fun loadBank(): LiveData<List<Bank>> {
        return repository.allBank
    }

    fun addAllBank() {
        repository.addAll()
    }

    fun getBankCount(): Int? {
        return repository.bankCount
    }

    fun getBanyById(id: Int): Bank{
        return repository.getBanyById(id)
    }

}