package project.framework.database.videmodel

import android.app.Application
import android.content.ClipData
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import project.framework.database.entity.Transaction
import project.framework.database.repositoryLiveData.TransactionRepository
import project.view.enum.TransactionTypes
import project.view.enum.TransactionTypes.DEPOST
import project.view.enum.TransactionTypes.REMOVAL

class TransactionViewModel(application: Application) : AndroidViewModel(application) {
    private var repository: TransactionRepository = TransactionRepository(application)
    private val selected = MutableLiveData<ClipData.Item>()

    fun select(item: ClipData.Item) {
        selected.value = item
    }

    fun insert(transaction: Transaction) {
        repository.insert(transaction)
    }


    fun update(transaction: Transaction) {
        Log.i("LOGGER", "" + transaction.toString())
        repository.update(transaction)
    }

    fun deleteAll() {
        //repository.delete()
    }

    fun delete(transaction: Transaction) {
        repository.delete(transaction)
    }

    fun getAll(): LiveData<List<Transaction>> {
        return loadTransaction()
    }

    private fun loadTransaction(): LiveData<List<Transaction>> {
        return repository.allTransaction
    }

    //    fun getTransactionAndBank(): LiveData<List<TAndB>> {
//        return repository.transactionJoinBank
//    }
    fun getTransactionAndBankCount(): Int {
        return repository.transactionJoinBankCount
    }

    fun addAllTransaction() {
        repository.addAllTransaction()
    }

    fun getTransactionCount(): Int? {
        return repository.transactionCount
    }

    fun getAllTransactionOrderByType(typeEnum: TransactionTypes): LiveData<List<Transaction>> {
        when (typeEnum) {
            DEPOST -> {
                return repository.getAllTransactionOrderByType("DEPOSIT")
            }
            REMOVAL -> {
                return repository.getAllTransactionOrderByType("REMOVAL")
            }
        }
    }

    fun getAllTransactionOrderByBankId(bankId: Int?): LiveData<List<Transaction>> {
        return repository.getAllTransactionOrderByBankId(bankId)
    }

    fun filterResultByInput(query: String): LiveData<List<Transaction>> {
        return repository.filterResultByInput(query)
    }

    fun getAllTransactionOrderByBankIdAndTypeId(bankId: Int, type: TransactionTypes): LiveData<List<Transaction>> {
        when (type) {
            DEPOST -> {
                return repository.getAllTransactionOrderByBankIdAndTypeId(bankId, "DEPOSIT")
            }
            REMOVAL -> {
                return repository.getAllTransactionOrderByBankIdAndTypeId(bankId, "REMOVAL")
            }
        }
    }


}