package project.framework.database.entity

import androidx.room.Embedded

data class TransactionJoinBank(
    @Embedded(prefix = "tr_")
    private var transaction: Transaction,
    @Embedded(prefix = "ba_")
    private var bank: Bank
) {
    override fun toString(): String {
        return "TransactionJoinBank(transaction=$transaction, bank=$bank)"
    }
}