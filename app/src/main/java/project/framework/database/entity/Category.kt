package project.framework.database.entity

import androidx.room.*

@Entity(
    tableName = "category",
    indices = [Index(value = ["category_id"], unique = true)]
)
data class Category(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "category_id") var id: Int?,
    var name: String,
    var color: String
) {
    @Ignore
    constructor(name: String, color: String) : this(null, name, color)

}