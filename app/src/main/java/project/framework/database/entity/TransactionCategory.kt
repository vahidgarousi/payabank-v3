package project.framework.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index

@Entity(
    tableName = "transaction_category",
    primaryKeys = ["transactionId", "categoryId"],
    foreignKeys = [
        ForeignKey(
            entity = Transaction::class,
            parentColumns = ["transaction_id"],
            childColumns = ["transactionId"]
        ),
        ForeignKey(
            entity = Category::class,
            parentColumns = ["category_id"],
            childColumns = ["categoryId"]
        )
    ],
    indices = [Index(value = ["transactionId", "categoryId"], unique = true)]
)
data class TransactionCategory(
    @ColumnInfo(name = "transactionId")
    var transactionId: Int,
    @ColumnInfo(name = "categoryId")
    var categoryId: Int
)