package project.framework.database.entity

import androidx.room.*
import java.io.Serializable

@Entity(
    tableName = "bank",
    indices = [Index(value = ["bank_id"], unique = true)]
)
data class Bank(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "bank_id") var id: Int?,
    var name: String?,
    var number: String?,
    var imageUrl: String?
) : Serializable {
    @Ignore
    constructor(name: String, number: String, imageUrl: String) : this(null, name, number, imageUrl)

    constructor() : this(null, null, null, null)

    override fun toString(): String {
        return "Bank(id=$id, name='$name', number='$number', imageUrl='$imageUrl')"
    }


}
