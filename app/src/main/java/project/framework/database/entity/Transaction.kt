package project.framework.database.entity

import androidx.room.*
import java.io.Serializable

import java.util.*

@Entity(
    tableName = "transaction",
    foreignKeys = [
        ForeignKey(
            entity = Bank::class,
            parentColumns = arrayOf("bank_id"),
            childColumns = arrayOf("bankId"),
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.NO_ACTION
        )
    ]
)
data class Transaction(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "transaction_id") val id: Int?,
    var name: String,
    var date: Date,
    @ColumnInfo(name = "bankId")
    var bankId: Int?,
    var type: String,
    var amount: String,
    var description: String
) : Serializable {
    @Ignore
    constructor(name: String, date: Date, bankId: Int, type: String, amount: String, description: String) : this(
        null, name, date, bankId, type, amount, description
    )

    override fun toString(): String {
        return "TransactionTypes(id=$id, name='$name', date=$date, bankId=$bankId, type='$type', amount='$amount', description='$description')"
    }


}