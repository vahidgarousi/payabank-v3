package project.framework.database.entity;

import androidx.room.Embedded;

public class TAndB {
    @Embedded(prefix = "tr_")
    private Transaction transaction;

    @Embedded(prefix = "ba_")
    private Bank bank;

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public String toString() {
        return "TAndB{" +
                "transaction=" + transaction +
                ", bank=" + bank +
                '}';
    }
}
