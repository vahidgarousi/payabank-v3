package project.framework.helper;

import android.app.Activity;
import android.app.TimePickerDialog;

import java.util.Calendar;

/**
 * private void openTimePicker() {
 *     VTimeHelper.openTimePicker(this, new TimePickerDialog.OnTimeSetListener() {
 *       @Override
 *       public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
 *         Toast.makeText(MainActivity.this, "" + hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
 *       }
 *     });
 *   }
 */
public class VTimeHelper {

  public static void openTimePicker(Activity activity, TimePickerDialog.OnTimeSetListener listener, int hour, int minute, boolean is24Format) {
    show(activity, listener, hour, minute, is24Format);
  }

  public static void openTimePicker(Activity activity, TimePickerDialog.OnTimeSetListener listener, int hour, int minute) {
    show(activity, listener, hour, minute, false);
  }

  public static void openTimePicker(Activity activity, TimePickerDialog.OnTimeSetListener listener, boolean is24Format) {
    Calendar calendar = Calendar.getInstance();
    int hour = calendar.get(Calendar.HOUR_OF_DAY);
    int minute = calendar.get(Calendar.MINUTE);
    show(activity, listener, hour, minute, is24Format);
  }

  public static void openTimePicker(Activity activity, TimePickerDialog.OnTimeSetListener listener) {
    Calendar calendar = Calendar.getInstance();
    int hour = calendar.get(Calendar.HOUR_OF_DAY);
    int minute = calendar.get(Calendar.MINUTE);
    show(activity, listener, hour, minute, false);
  }

  private static void show(Activity activity, TimePickerDialog.OnTimeSetListener listener, int hour, int minute, boolean is24Format) {

    new
      TimePickerDialog(activity, listener, hour, minute, false).show();

  }
}

//new TimePickerDialog.OnTimeSetListener() {
//@Override
//public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
//  Toast.makeText(this, "" + hourOfDay + ":" + minute, Toast.LENGTH_SHORT).show();
//  }
