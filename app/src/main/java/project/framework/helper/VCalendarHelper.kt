package project.framework.helper

import android.annotation.SuppressLint
import android.util.Log
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class VCalendarHelper {
    companion object {
        private val DAY_MS = 86400000
        private val months = intArrayOf(31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29)
        //leap year 1351
        private fun getMillisecondsFrom(date: String, format: String?): Long {
            var mFormat = format
            val mDateFromInput = date
            if (mFormat == null) {
                mFormat = "yyyy-MM-dd"
            }
            try {
                //val locale = Locale("fa-IR")
                val sdf = SimpleDateFormat(mFormat)
                val mDate: Date
                mDate = sdf.parse(mDateFromInput)

                return mDate.time
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            return 0L
        }

        private fun getJalaliFromMs(ms: Long): String {
            var days = ms / DAY_MS // 18035.8125
            var monthIndex = 10
            days -= 18
            var year = 1348
            while (true) {
                if (days > months[monthIndex]) {
                    days -= months[monthIndex].toLong()
                } else {
                    break
                }
                monthIndex++
                if (monthIndex == 12) {
                    if ((year - 1347) % 4 == 0) {
                        days -= 1
                    }
                    year++
                    monthIndex = 0
                }
            }
            return year.toString() + "/" + (monthIndex + 1) + "/" + days
        }

        fun g2j(date: String, format: String): String {
            return getJalaliFromMs(getMillisecondsFrom(date, format))
        }

        fun g2j(date: String): String {
            return getJalaliFromMs(getMillisecondsFrom(date, null))
        }

        @SuppressLint("SimpleDateFormat")
        fun getTime(date: Date): String = SimpleDateFormat("HH:mm").format(date)

        @SuppressLint("SimpleDateFormat")
        fun gteDateFromString(date: String) = SimpleDateFormat("yyyy-MM-dd").format(date)

        @SuppressLint("SimpleDateFormat")
        fun getDate(): Date {
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, 1)
            val format1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            Log.i("LOGGER", "cal.time" + cal.time)
            val formatted = format1.format(cal.time)
            Log.i("LOGGER", "formatted" + formatted)
            Log.i("LOGGER", "unFormatted" + format1.parse(formatted))

            val c = Calendar.getInstance().time
            return c
        }

        @SuppressLint("SimpleDateFormat")
        fun getYearMonthDayFromDate(date: Date): String? {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd")
            return g2j(dateFormat.format(date))
        }
    }
}
