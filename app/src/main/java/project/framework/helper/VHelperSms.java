package project.framework.helper;

import android.telephony.SmsManager;

/**
 * Created by VahidGarousi on 10/30/2017
 */

public  class VHelperSms {
    public static void sendSms(String destination, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(destination, null, message, null, null);
    }
}
