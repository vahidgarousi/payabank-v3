//package project.util.services
//
//import android.content.BroadcastReceiver
//import android.content.Context
//import android.content.Intent
//import android.telephony.SmsMessage
//import project.room.model.SMSModel
//
//
//class SmsReceiver(onTransactionUpdateListener: MyBroadcastListener) : BroadcastReceiver() {
//    private var onTransactionUpdateListener: MyBroadcastListener? = null
//
//    init {
//        this.onTransactionUpdateListener = onTransactionUpdateListener
//    }
//
//    interface MyBroadcastListener {
//        fun updateData(smsModel: SMSModel)
//    }
//
//    override fun onReceive(context: Context?, intent: Intent?) {
//        if (intent!!.action == "android.provider.Telephony.SMS_RECEIVED") {
//            val bundle = intent.extras           //---getBanyById the SMS message passed in---
//            val msgs: Array<SmsMessage?>
//            if (bundle != null) {
//                //---retrieve the SMS message received---
//                val pdus = bundle.getBanyById("pdus") as Array<*>
//                msgs = arrayOfNulls(pdus.size)
//                var smsModel: SMSModel? = null
//
//                val smsBuilder = StringBuilder()
//                for (i in 0 until msgs.size) {
//                    msgs[i] = SmsMessage.createFromPdu(pdus[i] as ByteArray)
//                    //val msg = msgs[i]!!!!
//                    val msgBody = msgs[i]!!.messageBody
//                    smsModel = SMSModel()
//                    if (msgBody != null && msgBody.isNotEmpty()) {
//                        smsBuilder.append(msgBody)
//
//                        smsModel.displayMessageBody = msgs[i]!!.displayMessageBody
//                        smsModel.displayOriginatingAddress = msgs[i]!!.displayOriginatingAddress
//                        smsModel.emailBody = msgs[i]!!.emailBody
//                        smsModel.emailFrom = msgs[i]!!.emailFrom
//                        smsModel.indexOnIcc = msgs[i]!!.indexOnIcc
//                        smsModel.messageBody = smsBuilder.toString()
//                        //SmsMessage.MessageClass messageClass = msgs[i]!!.getMessageClass();
//                        //byte[] pdu = msgs[i]!!.getPdu();
//                        smsModel.protocolIdentifier = msgs[i]!!.protocolIdentifier
//                        smsModel.originatingAddress = msgs[i]!!.originatingAddress
//                        smsModel.pseudoSubject = msgs[i]!!.pseudoSubject
//                        smsModel.serviceCenterAddress = msgs[i]!!.serviceCenterAddress
//                        //byte[] userData = msgs[i]!!.getUserData();
//                        smsModel.status = msgs[i]!!.status
//                        smsModel.timestampMillis = msgs[i]!!.timestampMillis
//                    }
//                }
//
//                if (smsModel != null) {
//                    onTransactionUpdateListener!!.updateData(smsModel)
//                }
//            }
//        }
//    }
//}