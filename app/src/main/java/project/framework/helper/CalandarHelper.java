package project.framework.helper;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CalandarHelper {
    private static final int DAY_MS = 86400000;
    private static final int[] months = new int[]{31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29};

    //leap year 1351
    private long getMillisecondsFrom(String date, String format) {
        if (format == null) {
            format = "yyyy-MM-dd";
        }
        try {
            Locale locale = new Locale("fa-IR");
            SimpleDateFormat sdf = new SimpleDateFormat(format, locale);
            Date mDate;
            mDate = sdf.parse(date);
            Log.i(
                    "LOGGER", "g2j " +
                            date + " mDate.time" +
                            mDate.getTime()
            );
            return mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0L;
    }

    private String getJalaliFromMs(long ms) {
        long days = ms / DAY_MS; // 18035.8125
        int monthIndex = 10;
        days -= 18;
        int year = 1348;
        while (true) {
            if (days > months[monthIndex]) {
                days -= months[monthIndex];
            } else {
                break;
            }
            monthIndex++;
            if (monthIndex == 12) {
                if (((year - 1347) % 4) == 0) {
                    days -= 1;
                }
                year++;
                monthIndex = 0;
            }
        }

        return year + "/" + (monthIndex + 1) + "/" + days;
    }

    public String g2j(String date, String format) {
        return getJalaliFromMs(getMillisecondsFrom(date, format));
    }

    public String g2j(String date) {
        Log.i("LOGGER", "g2j " + date + "getMillisecondsFrom" +  getMillisecondsFrom(date, null));
        return getJalaliFromMs(getMillisecondsFrom(date, null));
    }

}
