package project.framework.helper

import android.content.Context
import android.os.Vibrator
import android.util.Log
import androidx.core.text.isDigitsOnly
import project.framework.core.VBase

class Common {

    companion object {
        fun removeThousandSeprator(price: String): String {
            var newPrice = price
            if (!newPrice.isDigitsOnly()) {
                newPrice = str2Number(newPrice)
            }

            if (newPrice.contains(",")) {
                return newPrice.replace(",", "")
            }
            return newPrice
        }

        fun str2Number(value: String): String {
            return value.replace("[^\\d.]".toRegex(), "")
        }

        fun vibrate(millisecons: Long) {
            val vibrator = VBase.getContext().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            vibrator.vibrate(millisecons)
        }

        fun str2ThusandSeprator(value: String): String {
            if (value.contains(",")){
                return value
            }
            if (value.length > 3) {
                return String.format("%,d", value.toInt())
            }
            return value
        }

        fun vLog(message: String) {
            Log.i("LOGGER", " " + message)
        }
    }

}