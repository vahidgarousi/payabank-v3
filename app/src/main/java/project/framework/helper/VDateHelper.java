package project.framework.helper;

import android.app.Activity;
import android.app.DatePickerDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * private void openDatePicker() {
 *     VDateHelper.openDatePicker(this, new DatePickerDialog.OnDateSetListener() {
 *       @Override
 *       public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
 *         Toast.makeText(MainActivity.this, "" + year + "/" + "/" + month + "/" + dayOfMonth, Toast.LENGTH_SHORT).show();
 *         openTimePicker();
 *       }
 *     });
 *   }
 */
public class VDateHelper {

  public static void openDatePicker(Activity activity, DatePickerDialog.OnDateSetListener listener, int year, int month, int day) {
    show(activity, listener, year, month - 1, day);
  }


  public static void openDatePicker(Activity activity, DatePickerDialog.OnDateSetListener listener) {
    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH);
    int day = calendar.get(Calendar.DAY_OF_MONTH);
    show(activity, listener, year, month, day);
  }

  private static void show(Activity activity, DatePickerDialog.OnDateSetListener listener, int year, int month, int day) {
    new
      DatePickerDialog(activity, listener, year, month, day).show();
  }

  public static Date getDate(int diff){
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.add(Calendar.MILLISECOND,diff);
    return calendar.getTime();
  }
}