package project.framework.helper

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import project.framework.core.VBase

class VRequestHelper {

    companion object {
        fun hasPermissions(context: Context?, vararg permissions: String): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
                for (permission in permissions) {
                    if (ActivityCompat.checkSelfPermission(
                            context!!,
                            permission
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        return false
                    }
                }
            }
            return true
        }

        fun requestPermissions() {
            val Permissions =
                arrayOf<String>(
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.READ_SMS,
                    Manifest.permission.RECEIVE_SMS
                )
            val Permission_All = 1
            if (!VRequestHelper.hasPermissions(VBase.getContext(), *Permissions)) {
                ActivityCompat.requestPermissions(VBase.getCurrentActivity(), Permissions, Permission_All)
            }
        }
    }
}
